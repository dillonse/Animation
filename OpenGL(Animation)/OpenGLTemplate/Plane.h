#pragma once
#include <glm\vec3.hpp>
#include "instancee.h"

class Plane
{
public:
	instance inst;
	float restitution_coef = 0.5;
	glm::vec3 position=glm::vec3(0,-10,0);
	glm::vec3 normal = glm::vec3(1,0,0);
	void Render();
};
