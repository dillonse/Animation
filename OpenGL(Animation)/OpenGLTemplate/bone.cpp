#include "bone.h"

void skeleton::initMSM()
{
	//create a node in the MSM per animation
	for (int i = 0; i < animations.size(); i++) {
		MSMNode node;
		node.name = animations[i].name;
		node.animation_index = i;
		msm.nodes.push_back(node);
	}
}

AABB::AABB(glm::vec3 min, glm::vec3 max)
{
	//set the centre as the midpoint between min and max
	center = (max + min)*0.5f;
	//set the length as the abs diff between the min and max
	size = glm::abs(max - min);
	//set the axisA and axis B by comparing the size vector
	axisA = glm::vec3(
		size.x > size.y&&size.x > size.z ? 1.0f : 0.0f,
		size.y > size.z&&size.y > size.z ? 1.0f : 0.0f,
		size.z > size.x&&size.z > size.y ? 1.0f : 0.0f);
	axisB = glm::vec3(
		(size.x > size.y) ^ (size.x > size.z) ? 1.0f : 0.0f,
		(size.y > size.x) ^ (size.y > size.z) ? 1.0f : 0.0f,
		(size.z > size.x) ^ (size.z > size.y) ? 1.0f : 0.0f
	);
}
