#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "material.h"
#include "bone.h"
struct model {
	//data buffers
	std::string name = "";
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> colors;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> UVs;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;
	std::vector<std::vector<std::pair<float, float>>> boneIDWeights;
	std::vector<glm::vec4> boneWeightsContigious;
	std::vector<glm::vec4> boneIndicesContigious;
	std::vector<bone> bones;
	std::vector<glm::mat4> boneOffsetMatricesContigious;
	//data points
	glm::vec3 centroid=glm::vec3(0);
	//data memory flags
	GLuint verticesID=-1;
	GLuint normalsID=-1;
	GLuint UVsID=-1;
	GLuint tangentsID=-1;
	GLuint bitangetsID=-1;
	GLuint colorsID = -1;
	GLuint boneIDsID = -1;
	GLuint boneWeightsID = -1;
	void computeTangentBasis();
	void swapYZ();
	void computeCentroid();
	void flipFaceOrientation();
	void scale(glm::vec3 scale);
	void translate(glm::vec3 translation);
	bool isPositive();
};