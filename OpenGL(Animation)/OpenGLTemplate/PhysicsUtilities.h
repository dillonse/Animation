#pragma once
#include <glm\vec3.hpp>
#include "Particle.h"
namespace intersection {
	bool isPenetrating(Particle& p,glm::vec3& tpoint,glm::vec3& tnormal);
};

namespace response {
	glm::vec3 impulse(Particle& p,glm::vec3& normal,glm::vec3& tangent,float restitutionCoeff);
	glm::vec3 forces(Particle& p, glm::vec3& normal,float penetrationCoeff);
	void postProcessing(Particle& p,glm::vec3& position ,glm::vec3& normal,float restitutionCoeff);
}
