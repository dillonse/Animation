//OBJ loader based on online tutorial code
#ifndef _AUXILARYIO_H
#define AUXILARYIO_H
#include <windows.h>
#include <vector>
#include <glm\glm.hpp>
#include "model.h"
#include "bone.h"

bool load(const char * path, std::vector <glm::vec3> & out_vertices, std::vector <glm::vec3> & out_normals,std::vector <glm::vec2> & out_uvs);
bool load(const char* path, std::vector<model>& mod, std::vector<material>& mats);
bool load(const char* path,std::vector<model>& mod, skeleton* skel);
bool loadAnimations(std::vector<std::string> paths, std::vector<model>& mods, skeleton* skel);
bool loadHierarchy(const char* path,const char* hpath, std::vector<model>& models,skeleton& skeleton);
bool loadHand(const char* path, std::vector<model>&mods, skeleton& skeleton);
bool loadAnimationList(const char* path, std::vector<std::string>& paths);
#endif