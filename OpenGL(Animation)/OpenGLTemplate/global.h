#ifndef  _GLOBAL_H
#define _GLOBAL_H

#include <glm/matrix.hpp>
#include <GL\glew.h>
#include "light.h"
#include "instancee.h"
namespace camera{
	extern glm::mat4 ViewMatrix;
	extern glm::mat4 ProjectionMatrix;
	extern GLfloat ratio;
	extern GLfloat FOV;
	extern GLfloat near_dist;
	extern GLfloat far_dist;
	extern glm::vec3 position;
	extern glm::mat4 ViewMatrixNoTranslation;
}

namespace viewport {
	extern int width;
	extern int height;
}

namespace lights {
	 extern light light0;
	 extern light light1;
}

namespace controls {
	//alpharithmetics
	extern GLboolean Key_A;
	extern GLboolean Key_D;
	extern GLboolean Key_W;
	extern GLboolean Key_S;
	extern GLboolean Key_I;
	extern GLboolean Key_K;
	extern GLboolean Key_J;
	extern GLboolean Key_L;
	extern GLboolean Key_U;
	extern GLboolean Key_O;
	extern GLboolean Key_SPACE;
	extern GLboolean Key_8;
	extern GLboolean Key_2;
	//specials
	extern GLboolean Key_Left;
	extern GLboolean Key_Right;
	extern GLboolean Key_Up;
	extern GLboolean Key_Down;
	extern GLboolean Key_plus;
	extern GLboolean Key_minus;
	extern GLboolean Key_divide;
	//mouse (miawww)
	extern GLboolean MouseLeft;
	extern GLboolean MouseRight;
	extern int MousePositionX;
	extern int MousePositionY;
}

namespace time {
	extern GLfloat previousFrameTime;
	extern GLfloat currentFrameTime;
	extern GLfloat deltaTime;
}

namespace demo {
	extern int selection_index;
	extern int rotate_direction;
}

namespace animations {
	extern int maxNumBones;
	extern float currentAnimationDuration;
}
#endif