#pragma once
#include <glm\matrix.hpp>
#include <vector>
namespace Spline {
	glm::vec3 CatmullRom(float t,glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
	glm::vec3 CatmullRom(float& t, std::vector<glm::vec3> path);
};