#pragma once
#include "global.h"

void initInput(skeleton& skel) {
	//running-idle
	skel.msm.initTransition("idle", "running");
	skel.msm.initTransition("running", "idle");
	//running_b-idle
	skel.msm.initTransition("idle", "running_backward");
	skel.msm.initTransition("running_backward", "idle");
	//lstrage-idle
	skel.msm.initTransition("idle", "left_strafe");
	skel.msm.initTransition("left_strafe", "idle");
	//rstrafe-idle
	skel.msm.initTransition("idle", "right_strafe");
	skel.msm.initTransition("right_strafe", "idle");
	//run-lstrafe
	skel.msm.initTransition("left_strafe", "running");
	skel.msm.initTransition("running", "left_strafe");
	//run-rstrafe
	skel.msm.initTransition("right_strafe", "running");
	skel.msm.initTransition("running", "right_strafe");
	//brun-lstrafe
	skel.msm.initTransition("left_strafe", "running_backward");
	skel.msm.initTransition("running_backward", "left_strafe");
	//brun-rstrafe
	skel.msm.initTransition("right_strafe", "running_backward");
	skel.msm.initTransition("running_backward", "right_strafe");
	//running-jump
	skel.msm.initTransition("running", "running_jump");
	skel.msm.initTransition("running_jump", "running");
	skel.msm.setTransitionExitTime("running_jump", "running", 0.75f);
	//jump idle
	skel.msm.initTransition("running_jump", "idle");
	skel.msm.setTransitionExitTime("running_jump", "idle", 0.75f);
	//running rrturn
	skel.msm.initTransition("running", "running_right_turn");
	skel.msm.initTransition("running_right_turn", "running");
	//skel.msm.setCompolsury("running_right_turn", "running");
	//running lturn
	skel.msm.initTransition("running", "running_left_turn");
	skel.msm.initTransition("running_left_turn", "running");
	//skel.msm.setCompolsury("running_left_turn", "running");
	//death
	skel.msm.initTransition("running", "Standing_React_Death_Right");
	skel.msm.initTransition("idle", "Standing_React_Death_Right");
	skel.msm.initTransition("running_jump", "Standing_React_Death_Right");
	skel.msm.initTransition("left_strafe", "Standing_React_Death_Right");
	skel.msm.initTransition("right_strafe", "Standing_React_Death_Right");
	skel.msm.initTransition("running_backward", "Standing_React_Death_Right");
	skel.msm.initTransition("running_right_turn", "Standing_React_Death_Right");
	skel.msm.initTransition("running_left_turn", "Standing_React_Death_Right");
	//win
	skel.msm.initTransition("running", "victory");
	skel.msm.initTransition("idle", "victory");
	skel.msm.initTransition("running_jump", "victory");
	skel.msm.initTransition("left_strafe", "victory");
	skel.msm.initTransition("right_strafe", "victory");
	skel.msm.initTransition("running_backward", "victory");
	//set current state to idle
	skel.msm.setCurrentNode("idle");
}

void handleInput(skeleton& skel) {
	//movement
	static bool verticalsFirst = false;
	if (controls::Key_W || controls::Key_S) {
		if (!controls::Key_A && !controls::Key_D) {
			verticalsFirst = true;
		}
	}
	if (controls::Key_A || controls::Key_D) {
		if (!controls::Key_W && !controls::Key_S) {
			verticalsFirst = false;
		}
	}

	if (controls::Key_W) {
		skel.msm.setTransition("idle", "running", true);
		skel.msm.setTransition("running", "idle", false);
		if (controls::MouseLeft) {

		}
		if (verticalsFirst)
			if (controls::Key_A) {
				skel.msm.setTransition("running", "left_strafe", true);
				skel.msm.setTransition("running", "right_strafe", false);
			}
			else if (controls::Key_D) {
				skel.msm.setTransition("running", "right_strafe", true);
				skel.msm.setTransition("running", "left_strafe", false);
			
			}
			else {
				skel.msm.setTransition("running", "left_strafe", false);
				skel.msm.setTransition("running", "right_strafe", false);
			}
	}
	else {
		skel.msm.setTransition("idle", "running", false);
		skel.msm.setTransition("running", "idle", true);
		skel.msm.setTransition("running", "left_strafe", false);
		skel.msm.setTransition("running", "right_strafe", false);
	}
	if (controls::Key_S) {
		skel.msm.setTransition("idle", "running_backward",true);
		skel.msm.setTransition("running_backward", "idle", false);
		if (verticalsFirst)
			if (controls::Key_A) {
				skel.msm.setTransition("running_backward", "left_strafe", true);
				skel.msm.setTransition("running_backward", "right_strafe", false);
			}
			else if (controls::Key_D) {
				skel.msm.setTransition("running_backward", "right_strafe", true);
				skel.msm.setTransition("running_backward", "left_strafe", false);
			}
			else {
				skel.msm.setTransition("running_backward", "left_strafe", false);
				skel.msm.setTransition("running_backward", "right_strafe", false);
			}
	}
	else {
		skel.msm.setTransition("idle", "running_backward", false);
		skel.msm.setTransition("running_backward", "idle", true);
		skel.msm.setTransition("running_backward", "left_strafe", false);
		skel.msm.setTransition("running_backward", "right_strafe", false);
	}
	if (controls::Key_A) {
		skel.msm.setTransition("idle", "left_strafe", true);
		skel.msm.setTransition("left_strafe", "idle", false);
		if (!verticalsFirst)
			if (controls::Key_W) {
				skel.msm.setTransition("left_strafe", "running", true);
				skel.msm.setTransition("left_strafe", "running_backward", false);
			}
			else if (controls::Key_S) {
				skel.msm.setTransition("left_strafe", "running_backward", true);
				skel.msm.setTransition("left_strafe", "running", false);
			}
			else {
				skel.msm.setTransition("left_strafe", "running_backward", false);
				skel.msm.setTransition("left_strafe", "running", false);
			}
		
	}
	else {
		skel.msm.setTransition("idle", "left_strafe", false);
		skel.msm.setTransition("left_strafe", "idle", true);
		skel.msm.setTransition("left_strafe", "running", false);
	}
	if (controls::Key_D) {
		skel.msm.setTransition("idle", "right_strafe", true);
		skel.msm.setTransition("right_strafe", "idle", false);
		if (!verticalsFirst)
			if (controls::Key_W) {
				skel.msm.setTransition("right_strafe", "running", true);
				skel.msm.setTransition("right_strafe", "running_backward", false);
			}
			else if (controls::Key_S) {
				skel.msm.setTransition("right_strafe", "running_backward", true);
				skel.msm.setTransition("right_strafe", "running", false);
			}
			else {
				skel.msm.setTransition("right_strafe", "running_backward", false);
				skel.msm.setTransition("right_strafe", "running", false);
			}
	}
	else {
		skel.msm.setTransition("idle", "right_strafe", false);
		skel.msm.setTransition("right_strafe", "idle", true);
		skel.msm.setTransition("right_strafe", "running", false);
	}
	if (controls::Key_SPACE) {
		skel.msm.setTransition("running", "running_jump", true);
		skel.msm.setTransition("running_jump", "running", false);
	}
	else {
		skel.msm.setTransition("running", "running_jump", false);
		if (controls::Key_W) {
			skel.msm.setTransition("running_jump", "running", true);
			skel.msm.setTransition("running_jump", "idle", false);
		}
		else {
			skel.msm.setTransition("running_jump", "idle", true);
			skel.msm.setTransition("running_jump", "running", false);
		}
	}
	if (controls::MouseLeft) {
		skel.msm.setTransition("running", "running_right_turn", true);
		skel.msm.setTransition("running_right_turn", "running", false);
	}
	else {
		skel.msm.setTransition("running", "running_right_turn", false);
		skel.msm.setTransition("running_right_turn", "running", true);
	}
	if (controls::MouseRight) {
		skel.msm.setTransition("running", "running_left_turn", true);
		skel.msm.setTransition("running_left_turn", "running", false);
	}
	else {
		skel.msm.setTransition("running", "running_left_turn", false);
		skel.msm.setTransition("running_left_turn", "running", true);
	}
	if (controls::Key_2) {
		skel.msm.setTransition("running", "Standing_React_Death_Right",true);
		skel.msm.setTransition("idle", "Standing_React_Death_Right",true);
		skel.msm.setTransition("running_jump", "Standing_React_Death_Right",true);
		skel.msm.setTransition("left_strafe", "Standing_React_Death_Right",true);
		skel.msm.setTransition("right_strafe", "Standing_React_Death_Right",true);
		skel.msm.setTransition("running_backward", "Standing_React_Death_Right",true);
		skel.msm.setTransition("running_right_turn", "Standing_React_Death_Right", true);
		skel.msm.setTransition("running_left_turn", "Standing_React_Death_Right", true);
	}
	if (controls::Key_8) {
		skel.msm.setTransition("running", "victory", true);
		skel.msm.setTransition("idle", "victory", true);
		skel.msm.setTransition("running_jump", "victory", true);
		skel.msm.setTransition("left_strafe", "victory", true);
		skel.msm.setTransition("right_strafe", "victory", true);
		skel.msm.setTransition("running_backward", "victory", true);
		skel.msm.setTransition("running_right_turn", "victory",true);
		skel.msm.setTransition("running_left_turn", "victory",true);
	}
}