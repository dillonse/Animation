#include "Splines.h"

glm::vec3 Spline::CatmullRom(float t, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	return 0.5f*(2.0f*p1 +
		(-p0 + p2)*t +
		(2.0f*p0 - 5.0f*p1 + 4.0f*p2 - p3)*t*t +
		(-p0 + 3.0f * p1 - 3.0f * p2 + p3)*t*t*t);
}

glm::vec3 Spline::CatmullRom(float& t, std::vector<glm::vec3> path) {
	//get the vector size
	int size = path.size();
	//create the appropriate index and t
	int index = glm::floor(t);
	float tt = t - glm::floor(t);
	if (index > size - 4) {
		index = 0;
		t = 0;
		tt = 0;
	}
	return Spline::CatmullRom(tt, path[index], path[index + 1], path[index+2],path[index+3]);
}
