#pragma once
#include <vector>
#include "instancee.h"
#include "global.h"
#include <glm\gtc\matrix_transform.hpp>

class Pipe {
public:
	Pipe(glm::ivec2 col1, glm::ivec2 col2);
	float flow=0.0f;
	float flowDifferential = 0.0f;
	glm::ivec2 col1;
	glm::ivec2 col2;
};

class Column {
	//physics
public:
	float volume=1.0f;
	float externalPressure=0.0f;
	float volumeDifferential = 0.0f;
	float calculateHeight(float dx,float dy);
	float calculateStaticPresure(float dx, float dy);
	float calculateTotalPresure(float dx,float dy);

	//rendering
	instance inst;
	void Render(int i,int j,float dx, float dy);
};

class VolumeModel
{
public:
	VolumeModel(instance inst,int dimension = 2,float dx=1.0f,float dy=1.0f);
	VolumeModel() {};
	float calculatePipeAcceleration(glm::ivec2 col1, glm::ivec2 col2);
	float calculatePipeFlowDifferential(glm::ivec2 col1, glm::ivec2 col2);
	float calculateColumnVerticalVelocity(glm::ivec2 col1);
	glm::vec2 calculateColumnHorizontalVelocity(glm::ivec2 col1);
	void printVolumes();
	void updatePipeFlows();
	void updateColumnVolumes();
	std::vector<std::vector<Column>> columns;
	std::vector<Pipe> pipes;
	int dimension;
	float dx;
	float dy;
	void Render();
	float totalVolume = 0.0f;
};