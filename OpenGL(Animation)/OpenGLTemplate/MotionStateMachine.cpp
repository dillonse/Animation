#include "MotionStateMachine.h"
#include "global.h"

bool MotionStateMachine::setCurrentNode(std::string n)
{
	std::vector<MSMNode>::iterator res = std::find_if(nodes.begin(), nodes.end(), [n](auto elem) {
		return n == elem.name;
	});
	if (res == nodes.end())return false;
	currentNode= std::distance(nodes.begin(), res);
	previousNode = currentNode;
	return true;
}

bool MotionStateMachine::initTransition(std::string n1, std::string n2)
{
	//find the indices of the two nodes
	std::vector<MSMNode>::iterator res1 = std::find_if(nodes.begin(), nodes.end(), [n1](auto elem) {
		return n1 == elem.name;
	});
	if (res1 == nodes.end())return false;

	std::vector<MSMNode>::iterator res2 = std::find_if(nodes.begin(), nodes.end(), [n2](auto elem) {
		return n2 == elem.name;
	});
	if (res2 == nodes.end())return false;
	int index1 = std::distance(nodes.begin(), res1);
	int index2 = std::distance(nodes.begin(), res2);
	//add the index2 to the outcomings of node1
	nodes[index1].outcoming.push_back(index2);
	nodes[index1].out_conditions.push_back(false);
	nodes[index1].out_time.push_back(0.25f);
	nodes[index1].exitTime.push_back(0.0f);
	//add the index1 to the incomings of node2
	nodes[index2].incoming.push_back(index1);
	return true;
}

bool MotionStateMachine::setTransition(std::string n1, std::string n2, bool flag)
{
	//find the indices of the two nodes
	std::vector<MSMNode>::iterator res1 = std::find_if(nodes.begin(), nodes.end(), [n1](auto elem) {
		return n1 == elem.name;
	});
	if (res1 == nodes.end())return false;

	int index1 = std::distance(nodes.begin(), res1);
	//check for the index2 in the outcoming nodes
	for (int i = 0; i < nodes[index1].outcoming.size(); i++) {
		if (nodes[nodes[index1].outcoming[i]].name == n2) {
			nodes[index1].out_conditions[i] = flag;
			return true;
		}
	}

	return false;
}

bool MotionStateMachine::setTransitionDuration(std::string n1, std::string n2, float time)
{
	//find the indices of the two nodes
	std::vector<MSMNode>::iterator res1 = std::find_if(nodes.begin(), nodes.end(), [n1](auto elem) {
		return n1 == elem.name;
	});
	if (res1 == nodes.end())return false;

	int index1 = std::distance(nodes.begin(), res1);
	//check for the index2 in the outcoming nodes
	for (int i = 0; i < nodes[index1].outcoming.size(); i++) {
		if (nodes[nodes[index1].outcoming[i]].name == n2) {
			nodes[index1].out_time[i] = time;
			return true;
		}
	}

	return false;
}

bool MotionStateMachine::setTransitionExitTime(std::string n1, std::string n2, float time)
{
	//find the indices of the two nodes
	std::vector<MSMNode>::iterator res1 = std::find_if(nodes.begin(), nodes.end(), [n1](auto elem) {
		return n1 == elem.name;
	});
	if (res1 == nodes.end())return false;

	int index1 = std::distance(nodes.begin(), res1);
	//check for the index2 in the outcoming nodes
	for (int i = 0; i < nodes[index1].outcoming.size(); i++) {
		if (nodes[nodes[index1].outcoming[i]].name == n2) {
			nodes[index1].exitTime[i] = time;
			return true;
		}
	}

	return false;
}

bool MotionStateMachine::setCompolsury(std::string n1, std::string n2)
{
	//find the indices of the two nodes
	std::vector<MSMNode>::iterator res1 = std::find_if(nodes.begin(), nodes.end(), [n1](auto elem) {
		return n1 == elem.name;
	});
	if (res1 == nodes.end())return false;

	std::vector<MSMNode>::iterator res2 = std::find_if(nodes.begin(), nodes.end(), [n2](auto elem) {
		return n2 == elem.name;
	});
	if (res2 == nodes.end())return false;
	int index1 = std::distance(nodes.begin(), res1);
	int index2 = std::distance(nodes.begin(), res2);
	nodes[index1].compolsury = index2;
	return false;
}

void MotionStateMachine::update()
{
	timer += time::deltaTime;
	//check if any of the out conditions of the current node is true
	if (nodes[currentNode].compolsury != -1) {
		if (glm::abs(timer-animations::currentAnimationDuration)<0.1f) {
			previousNode = currentNode;
			timer = 0.0f;
			currentNode = nodes[currentNode].compolsury;
			return;
		}
	}
	for (int i = 0; i < nodes[currentNode].out_conditions.size(); i++) {
		if (nodes[currentNode].out_conditions[i]
			&& (transitionPercentage==1.0f||transitionPercentage==0.0f)
			&&nodes[currentNode].exitTime[i]<timer){
			previousNode = currentNode;
			timer = 0.0f;
			currentNode = nodes[currentNode].outcoming[i];
			break;
		}
	}
}
