#pragma once
#include "model.h"
#include "material.h"
#include "bone.h"

struct instance {
	std::string name;
	std::vector<model> mods;
	std::vector<material> mats;
	model* mod=NULL;
	model* modTpose = NULL;
	skeleton* skel = NULL;
	material * mat = NULL;
	std::string previousAnimationName = "";
	int previousAnimationIndex = -1;
	bool isPlayer = false;
	glm::vec3 position;
	glm::vec3 eulerAngles;
	glm::vec4 q=glm::vec4(glm::cos(0),glm::sin(0.0f)*glm::vec3(0,1,0));
	glm::mat4 transform = glm::mat4(1.0);
	glm::mat4 rotate(GLfloat rotX,GLfloat rotY,GLfloat rotZ);
	glm::mat4 scale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ);
	glm::mat4 rotateAround(GLfloat rotX, GLfloat rotY, GLfloat rotZ,glm::vec3 pivot);
	glm::mat4 translate(GLfloat transX, GLfloat transY, GLfloat transZ);
	glm::mat4 setPosition(glm::vec3 position);
	glm::vec3 up=glm::vec3(0,1,0);
	glm::vec3 right=glm::vec3(1,0,0);
	glm::vec3 forward=glm::vec3(0,0,-1);
	float time = 0.0f;
	void BindMaterial();
	void BindModel();
	void updateSkeleton(std::string rootName="Hips");
	void updateSkeletonRec(skeletonNode& node);
	void UpdateNode(std::string nodeName,glm::mat4 trans);
	void UpdateAnimation(std::string animationName);
	void UpdateAnimation();
	void updateBoneChildren(skeletonNode& node, glm::mat4& trans);
	void updateBone(skeletonNode& node, glm::mat4& trans);
	void updateIKCCD3D(skeletonNode& node,skeletonNode& root, glm::vec3 endPosition);
	void Render();
	void RenderWithoutMaterials();
	void RotateEuler();
	void Yaw(float angle);
	void Pitch(float angle);
	void Roll(float angle);
	float timer1=0.0f;
	float timer2 = 0.0f;
	float previousAnimTime = 0.0f;
};