#include "Plane.h"
#include <glm/gtx/transform.hpp>
//render implementation based on http://gamedev.stackexchange.com/questions/117262/how-do-you-build-a-rotation-matrix-from-a-normalized-vector
void Plane::Render() {
	//create the rows of the matrix
	glm::vec3 up = normal; //assume normal is upish
	glm::vec3 forward = glm::normalize(glm::cross(up, glm::vec3(1, 0, 0)));
	glm::vec3 right = glm::normalize(glm::cross(up, forward));
	inst.transform = glm::mat4(glm::vec4(right, 0.0f), glm::vec4(up, 0.0f), glm::vec4(forward, 0.0f), glm::vec4(0, 0, 0, 1.0f));
	inst.transform = glm::translate(inst.transform, position);
	inst.Render();
}