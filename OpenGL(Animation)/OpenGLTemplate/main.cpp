
//fan model from https://www.cgtrader.com/free-3d-models/exterior/industrial/industrial-fan

#include <Windows.h>
#include "debug.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "textfile.h"
#include "AuxilaryIO.h"
#include "VBO.h"
#include "textures.h"
#include "shaders.h"
#include "model.h"
#include "instancee.h"
#include <string>
#include <tuple>
#include "global.h"
#include "bone.h"
#include "Splines.h"
#include "InputHandler.h"
//Physics
#include "Rigidbody.h"
#include "RigidbodySystem.h"
#include "ParticleSystem.h"
#include "Contact.h"
#include "VolumeModel.h"
#include "SurfaceModel.h"
//particle systems
ParticleSystem particleSystem;
std::vector<instance> colliders;
//rigidbodies
RigidbodySystem rigidbodySystem;
Rigidbody* rigidbody;

//models
model skybox_mod;
model endpoint_mod;
model raft_mod;
model level_mod;

//model hand_mod;
//model hand_model;
model skeleton_mod;
std::vector<model> Beta_mod;
std::vector<model> alpha_mod;
//skeletons
//skeleton hand_skeleton;
skeleton skely_skeleton;
skeleton Beta_skel;
skeleton Alpha_skel;

//materials
material skybox_mat;
//material hand_mat;
material Beta_mat;
material endpoint_mat;
material raft_mat;
material level_mat;
material water_mat;
material Alpha_mat;
//instances
instance skybox_inst;
//instance hand_inst;
instance skeleton_inst;
instance alpha_inst;
instance endpoint_inst;
instance endpoint_inst2;
instance raft_inst;
instance level_inst;

VolumeModel volumeModel;
//surface model
SurfaceModel surfaceModel;

//instances
instance volume_inst;
instance particle_inst;
instance object_inst;

material volume_mat;
material particle_mat;
material object_mat;


//models
model volume_mod;
model particle_mod;
model object_mod;

//demo variables
int cameraRotationDirection = 1;
float cameraRotationSpeed = 50.0f;
int cameraZoomDirection = 1;
float cameraZoomSpeed=50.0f;
float plane_speed = 5.0f;
bool pause = true;

int max_selection_index = 20;;

glm::vec3 endpoint = glm::vec3(0.914, 1.68, 0.33);
glm::vec3 initPoint = glm::vec3(2.8f, 5.61f, 0.98f);
float t = 0.0f;
float animationSpeed = 0.1f;
int leftMidPhalIndex = 44;
int rightMidPhalIndex = 37;
int leftCarpalIndex = 72;
int rightCarpalIndex = 77;
std::vector<glm::vec3> point_vector;

bool animateCarpals = true;
bool useCurve = true;

void initPhysics() {
	volumeModel = VolumeModel(volume_inst, 175, 1.0f, 1.0f);
	surfaceModel = SurfaceModel(volumeModel);
	surfaceModel.forces[50][0] = 100000.0f;
	particle_inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(0.005, 0.005, 0.005));
	particleSystem.particleInstance = particle_inst;
	particleSystem.unaryForces.push_back(new GravityForce());
	object_inst.mod = new model(object_mod);
	object_inst.mat = &object_mat;
	rigidbody = new Rigidbody(object_inst, glm::vec3(5, 5, 5), object_inst, object_inst);
	rigidbodySystem.rigidBodies.push_back(*rigidbody);
	rigidbodySystem.unaryForces.push_back(new GravityForce());
	rigidbodySystem.UpdateGeometry(time::deltaTime);
	for (int i = 0; i < 500; i++) {
		volumeModel.updateColumnVolumes();
		surfaceModel.updateHeights();
		surfaceModel.updateNormals();
		surfaceModel.updateExternalPressures();
		surfaceModel.updateVerticalVelocities();
		surfaceModel.updateHorizontalVelocities();
		surfaceModel.updateParticleSystem(particleSystem);
		surfaceModel.updateCollisions(rigidbodySystem);
		std::vector<instance> inst;
		particleSystem.Update(0.01f, inst);
	}
}

float timer = 0.0f;
glm::mat4 cameraIntro = glm::translate(glm::mat4(1.0f), glm::vec3(0, -10,0))*glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0, 1, 0));//*glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(0, 0, 0));
void updatePhysics() {
	static float timeSpeed = 1.0f;
	time::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
	//time::pastDeltaTime = time::deltaTime;
	time::deltaTime = timeSpeed*((float)(time::currentFrameTime - time::previousFrameTime)) / 1000.0;
	//time::deltaTime *= 0.5f;
	time::previousFrameTime = time::currentFrameTime;
	//if (!pause) {
	timer += time::deltaTime;
	if (timer < 10.0f) {
		//camera::ViewMatrix[3] = glm::vec4(cameraPosIntro,1.0f);
	}
	if (timer > 45.0f) {
		controls::Key_8 = true;
	}
	surfaceModel.addForce(skeleton_inst.transform[3], 5.0f);
	if (surfaceModel.getHeight(skeleton_inst.transform[3]) > 2.0f) {
		controls::Key_2 = true;//DEATH!
		timeSpeed = 0.1;
	}
	if (surfaceModel.getHeight(alpha_inst.transform[3]) > 2.0f) {
		//alpha_inst.skel->msm.initTransition("injured_run", "falling_forward_death");
		alpha_inst.skel->msm.setTransition("injured_run", "falling_forward_death", true);
		//GOODBYE ARISSAAAA :'( 
	}
		volumeModel.updateColumnVolumes();
		volumeModel.updateColumnVolumes();
		volumeModel.updateColumnVolumes();
		volumeModel.updateColumnVolumes();
		volumeModel.updateColumnVolumes();
		surfaceModel.updateHeights();
		surfaceModel.updateNormals();
		surfaceModel.updateExternalPressures();
		surfaceModel.updateVerticalVelocities();
		surfaceModel.updateHorizontalVelocities();
		surfaceModel.updateParticleSystem(particleSystem);
		surfaceModel.updateCollisions(rigidbodySystem);
		std::vector<instance> inst;
		particleSystem.Update(0.01f, inst);
		rigidbodySystem.UpdateForcesTorque(time::deltaTime);
		rigidbodySystem.UpdateMomentums(time::deltaTime);
		rigidbodySystem.UpdateAuxilary(time::deltaTime);
		rigidbodySystem.UpdateGeometry(time::deltaTime);
		rigidbodySystem.UpdateBoundingVolumes(time::deltaTime);
	surfaceModel.addForce(skeleton_inst.transform[3], 0.0f);
		//message += "\n"+std::to_string(volumeModel.columns[0][0].volume)+"\n";
		//message += std::to_string(volumeModel.columns[1][0].volume) + "\n";
		//message += std::to_string(volumeModel.columns[2][0].volume) + "\n";
		//volumeModel.printVolumes();
	//}
}

//void updatePhysics() {
//	time::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
//	time::deltaTime = ((float)(time::currentFrameTime - time::previousFrameTime)) / 1000.0;
//	time::previousFrameTime = time::currentFrameTime;
//}

void init() {
	//enable backface culling
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	//set depth and transparency
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//set transformations
	camera::ViewMatrix = glm::lookAt(   // View matrix
		glm::vec3(0, 0, 0), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, -1), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);
	
	lights::light0.position = glm::vec3(15, 2, 16);
	lights::light0.BindShadowMap();
	point_vector.push_back(glm::vec3(0, 0, -1));
	point_vector.push_back(glm::vec3(-1.0f, -1, -1));
	point_vector.push_back(glm::vec3(-1.0f, 1, 1));
	point_vector.push_back(glm::vec3(4.0f, 0, 1));
	//loop
	point_vector.push_back(point_vector[0]);
	point_vector.push_back(point_vector[1]);
	point_vector.push_back(point_vector[2]);
}

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	camera::ratio = 1.0* w / h;
	// Reset the coordinate system before modifying
	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
	// Set the correct perspective.
	camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, camera::near_dist,camera::far_dist); //Projection matrix
	viewport::width = w;
	viewport::height = h;
}


bool animState = false;

void renderScene(void) {
	updatePhysics();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/*if (controls::Key_A) {
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, -camera::position);
		camera::ViewMatrixNoTranslation = glm::rotate(camera::ViewMatrixNoTranslation, -time::deltaTime, glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::rotate(camera::ViewMatrix, -time::deltaTime, glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, camera::position);

	}
	if (controls::Key_D) {
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, -camera::position);
		camera::ViewMatrixNoTranslation = glm::rotate(camera::ViewMatrixNoTranslation, time::deltaTime, glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::rotate(camera::ViewMatrix, time::deltaTime, glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, camera::position);
	}*/
	if (controls::Key_U) {
		camera::position += glm::vec3(0,time::deltaTime,0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0,time::deltaTime,0));
	}
	if (controls::Key_O) {
		camera::position += glm::vec3(0, -time::deltaTime, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, -time::deltaTime, 0));
	}
	if (controls::Key_I) {
		camera::position += glm::vec3(0, 0, time::deltaTime);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0,0,time::deltaTime));
	}
	if (controls::Key_K) {
		camera::position += glm::vec3(0, 0, -time::deltaTime);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0,-time::deltaTime));
	}
	if (controls::Key_L) {
		camera::position += glm::vec3(-time::deltaTime, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(-time::deltaTime, 0, 0));
	}
	if (controls::Key_J) {
		camera::position += glm::vec3(time::deltaTime, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(time::deltaTime, 0, 0));
	}
	//if (controls::Key_W || controls::Key_S) {
	//	camera::FOV += time::deltaTime*cameraZoomDirection*cameraZoomSpeed;
	//	camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, camera::near_dist, camera::far_dist); //Projection matrix
	//}

	if (controls::Key_8) {
		endpoint.y += time::deltaTime;
		std::cout << glm::to_string(endpoint) << std::endl;
	}
	if (controls::Key_2) {
		endpoint.y -= time::deltaTime;
	}

	if (controls::Key_Left) {
		endpoint.x += time::deltaTime;
	}
	if (controls::Key_Right) {
		endpoint.x-= time::deltaTime;
	}

	if (controls::Key_Up) {
		endpoint.z += time::deltaTime;
	}
	if (controls::Key_Down) {
		endpoint.z -= time::deltaTime;
	}
	if (controls::Key_plus) {
		animationSpeed += time::deltaTime;
		std::cout << "animation speed = " << animationSpeed << std::endl;
	}
	if (controls::Key_minus) {
		animationSpeed -= time::deltaTime;
		std::cout << "animation speed = " << animationSpeed << std::endl;
	}
	
	
	t + time::deltaTime;
	t += time::deltaTime*animationSpeed;
	//if(useCurve)
		//endpoint =initPoint+Spline::CatmullRom(t,point_vector);
	/*if (animateCarpals) {
		skeleton_inst.updateIKCCD3D(skeleton_inst.skel->nodes[rightCarpalIndex], endpoint);
		skeleton_inst.updateIKCCD3D(skeleton_inst.skel->nodes[leftCarpalIndex], glm::vec3(-1.0f, 1.0f, 1.0f)*endpoint);
	}
	else {
		skeleton_inst.updateIKCCD3D(skeleton_inst.skel->nodes[rightMidPhalIndex], endpoint);
		skeleton_inst.updateIKCCD3D(skeleton_inst.skel->nodes[leftMidPhalIndex], glm::vec3(-1.0f, 1.0f, 1.0f)*endpoint);
	}*/
	skeleton_inst.updateSkeleton();
	//skeleton_inst.updateIKCCD3D(skeleton_inst.skel->nodes[43], skeleton_inst.skel->nodes[40], endpoint);
	//render the skybox
	glDepthMask(GL_FALSE);
	skybox_inst.Render();
	glDepthMask(GL_TRUE);

	static float kay = 0.0f;
	kay += time::deltaTime*0.1f;
	handleInput(*skeleton_inst.skel);
	skeleton_inst.skel->msm.update();
	alpha_inst.skel->msm.update();
	alpha_inst.UpdateAnimation();
	skeleton_inst.UpdateAnimation();
	//skeleton_inst.UpdateNode("LeftLeg", glm::rotate(glm::mat4(1.0f), glm::radians(kay), glm::vec3(1, 0, 0)));
	//std::cout << "endpoint is " << glm::to_string(endpoint) << std::endl;
	static float viewOffsety = 0.0f;
	static float viewOffsetx = 0.0f;
	if (controls::Key_Up) {
		viewOffsety -= time::deltaTime;
	}
	if (controls::Key_Down) {
		viewOffsety += time::deltaTime;
	}
	if (controls::Key_Right) {
		viewOffsetx += time::deltaTime;
	}
	if (controls::Key_Left) {
		viewOffsetx -= time::deltaTime;
	}
	camera::ViewMatrix = camera::ViewMatrix*glm::translate(glm::mat4(1.0f), glm::vec3(viewOffsetx, viewOffsety, 0));
	if (timer < 10.0f) {
		camera::ViewMatrix = cameraIntro;
	}
	alpha_inst.Render();
	skeleton_inst.Render();
	raft_inst.Render();
	level_inst.Render();
	surfaceModel.Render(water_mat);
	particleSystem.Render();
	glutSwapBuffers();
}

void changeSkybox(std::string skyboxPath) {
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);
}


void processMouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN)
			controls::MouseLeft = true;
		else if (state == GLUT_UP)
			controls::MouseLeft = false;
	}
	if (button == GLUT_RIGHT_BUTTON) {
		if (state == GLUT_DOWN)
			controls::MouseRight = true;
		else if (state == GLUT_UP)
			controls::MouseRight = false;
	}
	//std::cout <<"Mouse controls to Major Sean:"<< controls::MouseLeft << "," << controls::MouseRight << std::endl;
}

void processMouseMotion(int x, int y) {
	static int previousX = 0;
	static int previousY = 0;
	previousX = controls::MousePositionX;
	previousY = controls::MousePositionY;
	if (controls::MouseRight) {
		//calculate the x interval and the y interval
		int deltaX = x - previousX;
		int deltaY = y - previousY;
		int epsilon = 10;
		
		if (deltaX > epsilon) {
		}
		else if (deltaX < epsilon) {
		}
		if (deltaY > epsilon) {
		}
		else if (deltaY < epsilon) {
		}
	}
	else if (controls::MouseLeft) {
		int deltaX = x - previousX;
		int deltaY = y - previousY;
		int epsilon = 10;

		if (deltaX > epsilon) {	
		}
		else if (deltaX < epsilon) {
		}
		if (deltaY > epsilon) {
		}
		else if (deltaY < epsilon) {
		}
	}
	else {
		//calculate the x interval and the y interval
		

	}
	controls::MousePositionX = x;
	controls::MousePositionY = y;
}

void ProcessMousePassiveMotion(int x, int y) {
	static int previousX = 0;
	static int previousY = 0;
	
	previousX = controls::MousePositionX;
	previousY = controls::MousePositionY;
	int deltaX = x - previousX;
	int deltaY = y - previousY;
	int epsilon = 10;

	if (deltaX > epsilon) {
		controls::MouseLeft = true;
	}
	else if (deltaX < -epsilon) {
		controls::MouseRight = true;
	}
	else {
		controls::MouseLeft = false;
		controls::MouseRight = false;
	}
	if (deltaY > epsilon) {
	}
	else if (deltaY < epsilon) {
	}
	controls::MousePositionX = x;
	controls::MousePositionY = y;
}

void processNormalKeys(unsigned char key, int x, int y) {
	std::string skyboxPath;
	std::vector<std::string> skyboxPaths;
	static float zoom = 45.0;
	switch (key)
	{
	case ' ':
		controls::Key_SPACE = true;
		animateCarpals = !animateCarpals;
		//Beta_skel.msm.setTransition("idle", "walking", true);
		break;
	case 27:
		exit(0);
	case 'a':
		controls::Key_A = true;
		cameraRotationDirection = -1;
		break;
	case 'd':
		controls::Key_D = true;
		cameraRotationDirection = 1;
		break;
	case 'w':
		controls::Key_W = true;
		cameraZoomDirection = 1;
		break;
	case 's':
		controls::Key_S = true;
		cameraZoomDirection = -1;
		break;
	case 'i':
		controls::Key_I = true;
		break;
	case 'j':
		controls::Key_J = true;
		break;
	case 'k':
		controls::Key_K = true;
		break;
	case 'l':
		controls::Key_L = true;
		break;
	case 'o':
		controls::Key_O = true;
		break;
	case 'u':
		controls::Key_U = true;
		break;
	case '0':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/textures/skybox0/";
		changeSkybox(skyboxPath);
		break;
	case '1':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/textures/skybox1/";
		changeSkybox(skyboxPath);

		break;
	case '2':
		controls::Key_2 = true;
		break;
	case '3':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/textures/skybox3/";
		changeSkybox(skyboxPath);
		break;
	case '4':
		demo::selection_index++;
		if (demo::selection_index == max_selection_index)
			demo::selection_index = 0;
		break;
	case '5':
		break;
	case '6':
		demo::selection_index--;
		if (demo::selection_index == -1)
			demo::selection_index = max_selection_index-1;
		break;
	case '7':
		break;
	case '8':
		controls::Key_8 = true;
		break;
	case '9':
		break;
	case '+':
		controls::Key_plus = true;
		break;
	case '-':
		controls::Key_minus = true;
		break;
	case '/':
		useCurve = !useCurve;
		controls::Key_divide = true;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}


void processNormalKeysUp(unsigned char key, int x, int y) {
	switch (key) {
	case 'a':
		controls::Key_A = false;
		break;
	case 'd':
		controls::Key_D = false;
		break;
	case 'w':
		controls::Key_W = false;
		break;
	case 's':
		controls::Key_S = false;
		break;
	case 'i':
		controls::Key_I = false;
		break;
	case 'j':
		controls::Key_J = false;
		break;
	case 'k':
		controls::Key_K = false;
		break;
	case 'l':
		controls::Key_L = false;
		break;
	case 'o':
		controls::Key_O = false;
		break;
	case 'u' :
		controls::Key_U = false;
		break;
	case ' ':
		controls::Key_SPACE = false;
		break;
	case '8':
		controls::Key_8 = false;
		break;
	case '2':
		controls::Key_2 = false;
	case '-':
		controls::Key_minus = false;
		break;
	case '+':
		controls::Key_plus = false;
		break;
	case '/':
		controls::Key_divide = false;
	}

}

void processSpecialKeys(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = true;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = true;
		break;
	case GLUT_KEY_UP:
		controls::Key_Up = true;
		break;
	case GLUT_KEY_DOWN:
		controls::Key_Down = true;
		//skeleton_inst.updateIK(skeleton_inst.skel->nodes[92], endpoint);
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void processSpecialKeysUp(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = false;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = false;
		break;
	case GLUT_KEY_UP:
		controls::Key_Up = false;
		break;
	case GLUT_KEY_DOWN:

		controls::Key_Down = false;
		break;
	default:
		break;
	}
}


void loadShaders() {
	//create a list of the shader paths
	std::vector<std::tuple<std::string, std::string>> shaderPaths;
	std::vector<GLuint> shaderIDs;
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/DebugShader/debug.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/DebugShader/debug.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/SkyboxShader/skybox.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/SkyboxShader/skybox.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/NormalMapShader/normalMap.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/NormalMapShader/normalMap.frag"));
	shaderPaths.push_back(std::make_tuple(
		"U:/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/FresnelShader/fresnel.vert",
		"U:/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/FresnelShader/fresnel.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureShader/texture.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureShader/texture.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ReflectionShader/reflection.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ReflectionShader/reflection.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/RefractionShader/refraction.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/RefractionShader/refraction.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/DepthMapShader/depth.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/DepthMapShader/depth.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/CollidersShader/collider.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/CollidersShader/collider.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ParticleShaderColors/particleColor.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/ParticleShaderColors/particleColor.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureShaderNoRotation/textureNoRotation.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/shaders/TextureShaderNoRotation/textureNoRotation.frag"));
	shaderPaths.push_back(std::make_tuple(
		"D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/shaders/ToonShader/base/base.vert",
		"D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/shaders/ToonShader/base/quantDiff.frag"));
	shaderPaths.push_back(std::make_tuple(
		"D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/shaders/ToonShader/outline/translateByNormal.vert",
		"D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/shaders/ToonShader/outline/outline.frag"));
	//load the shaders
	shaders::loadShaders(shaderPaths, shaderIDs);
	Beta_mat.DiffuseMapID = textures::loadTextureDDS("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/Akai_E_Espiritu/textures/akai_diffuse.png");
	Beta_mat.ShaderID = shaderIDs[18];

	Alpha_mat.ShaderID = shaderIDs[18];
	Alpha_mat.DiffuseMapID  = textures::loadTextureDDS("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/Arissa/textures/Arissa_DIFF_diffuse.png");
	//hand_mat.DiffuseMapID = textures::loadTextureDDS("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/textures/bone.png");
	//assign to global variables
	skybox_mat.ShaderID = shaderIDs[1];

	endpoint_mat.ShaderID = shaderIDs[0];
	/*hand_mat.ShaderID = shaderIDs[18];
	hand_shaderID1 = shaderIDs[18];
	hand_shaderID2 = shaderIDs[19];*/

	//skeleton_mat.ShaderID = shaderIDs[0];
	//NoRotationShaderID = shaderIDs[17];
	//NormalShaderID = shaderIDs[4];
	raft_mat.ShaderID = shaderIDs[0];
	level_inst.mats[0].ShaderID = shaderIDs[4];
	level_inst.mats[0].DiffuseMapID = textures::loadTextureDDS("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/textures/earth.png");

	level_inst.mats[1].ShaderID = shaderIDs[4];
	level_inst.mats[2].ShaderID = shaderIDs[4];
	level_inst.mats[2].color = glm::vec4(1, 1, 0, 1);
	level_inst.mats[3].ShaderID = shaderIDs[4];
	level_inst.mats[3].color = glm::vec4(0, 1, 0, 1);
	level_inst.mats[3].DiffuseMapID = textures::loadTextureDDS("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/textures/TempleRuin_01/3td_Concrete_mossy_01.png");
	level_inst.mats[4].ShaderID = shaderIDs[4];
	level_inst.mats[4].color = glm::vec4(0, 1, 0, 1);
	level_inst.mats[5].ShaderID = shaderIDs[4];
	level_inst.mats[5].color = glm::vec4(0, 1, 0, 1);

	water_mat.ShaderID = shaderIDs[3];
	//particle_mat = water_mat;
	//water_mat.DiffuseMapID = textures::loadTextureDDS("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/textures/TempleRuin_01/3td_Concrete_mossy_01.png");
	//water_mat.AlphaValue = 0.6;
}

void loadTexture() {
	//load the skybox maps
	std::string skyboxPath = "D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/textures/skybox1/";
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));

	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);
	water_mat.SkyboxMapID = skybox_mat.SkyboxMapID;
	water_mat.color = glm::vec3(-0.5, 0.2, 1);
	water_mat.AlphaValue = 0.7;
	//level_mat.DiffuseMapID = textures::loadTextureDDS("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/textures/TempleRuin_01/3td_Concrete_mossy_01.png");	
}

void loadObject() {
	//load the skybox cube
	if (!load("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/models/skybox.obj", skybox_mod.vertices, skybox_mod.normals, skybox_mod.UVs))
		exit(-1);
	skybox_mod.flipFaceOrientation();
	buffers::Load(skybox_mod);

	if (!load("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/raft.obj", raft_mod.vertices, raft_mod.normals, raft_mod.UVs))
		exit(-1);
	buffers::Load(raft_mod);

	if (!load("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/sphere.obj", particle_mod.vertices, particle_mod.normals, particle_mod.UVs)) {
		exit(-1);
	}
	buffers::Load(particle_mod);

	if (!load("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/level.obj", level_inst.mods, level_inst.mats))
		exit(-1);
	for (int i = 0; i < level_inst.mods.size(); i++) {
		buffers::Load(level_inst.mods[i]);
	}

	if (!load("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Animation)/OpenGLTemplate/models/sphere.obj", endpoint_mod.vertices, endpoint_mod.normals, endpoint_mod.UVs))
		exit(-1);
	//skybox_mod.flipFaceOrientation();
	buffers::Load(endpoint_mod);
	std::vector<std::string> animationPaths;
	//load Beta (.dae)
	if (!load("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/Akai_E_Espiritu/Akai_E_Espiritu.dae",Beta_mod,&Beta_skel))
		exit(-10);
	if (!loadAnimationList("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/Akai_E_Espiritu/animations/", animationPaths))
		exit(-20);
	if (!loadAnimations(animationPaths, Beta_mod, &Beta_skel))
		exit(-30);
	Beta_skel.initMSM();
	initInput(Beta_skel);
	for (int i = 0; i < Beta_mod.size(); i++) {
		//Beta_mod[i].scale(glm::vec3(0.001, 0.001, 0.001));
		buffers::Load(Beta_mod[i]);
	}

	animationPaths.clear();
	//load Beta (.dae)
	if (!load("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/Arissa/Arissa.dae", alpha_mod, &Alpha_skel))
		exit(-10);
	if (!loadAnimationList("D:/AnimationProject/OpenGL(Animation)/OpenGLTemplate/models/Arissa/animations/", animationPaths))
		exit(-20);
	if (!loadAnimations(animationPaths, alpha_mod, &Alpha_skel))
		exit(-30);
	Alpha_skel.initMSM();
	//initInput(Alpha_skel);
	for (int i = 0; i < alpha_mod.size(); i++) {
		//Beta_mod[i].scale(glm::vec3(0.001, 0.001, 0.001));
		buffers::Load(alpha_mod[i]);
	}

}



int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1600, 1200);
	glutCreateWindow("GLSL Example");

	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(processNormalKeysUp);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(processSpecialKeysUp);
	glutMotionFunc(processMouseMotion);
	glutMouseFunc(processMouseClick);
	glutPassiveMotionFunc(ProcessMousePassiveMotion);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glewInit();
	init();

	if (glewIsSupported("GL_VERSION_4_5"))
		printf("Ready for OpenGL 4.5\n");
	else {
		printf("OpenGL 4.5 not supported\n");
		//exit(1);
	}
	//load the auxilary data to memory and pass them to GPU
	std::cout << "loading shaders" << std::endl;
	loadObject();//first objects to get max num of bones, shaders next
	loadShaders();
	loadTexture();
	//connect the materials and models to instances
	///skybox
	skybox_inst.mod = &skybox_mod;
	skybox_inst.mat = &skybox_mat;
	skybox_inst.scale(50, 50, 50);

	particle_inst.mod = &particle_mod;
	particle_inst.mat = &water_mat;

	initPhysics();
	//hand_mat.color = glm::vec3(0, 1, 0);
//	hand_inst.mat = &hand_mat;
	//hand_inst.mod = &hand_model;
	//hand_inst.modTpose = new model(hand_model);
	/*hand_inst.skel = &hand_skeleton;
	hand_inst.scale(0.1, 0.1, 0.1);
	hand_inst.translate(0, 0, -1);*/
	//petyr_inst.scale(0.1, 0.1, 0.1);
	//petyr_inst.mat->AlphaValue = 0.1f;
	skeleton_inst.mods = Beta_mod;
	skeleton_inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(0.01, 0.01, 0.01));
	skeleton_inst.skel = &Beta_skel;
	//skeleton_inst.skel = &skely_skeleton;
	skeleton_inst.mat = &Beta_mat;
	//skeleton_inst.rotate(0,90.0f, 0);
	//skeleton_inst.translate(0, -5,-5);
	//{name = "RightHandPinky1" modelIndex = 2 boneIndex = 10 ...}
	skeleton_inst.updateSkeleton();
	//skeleton_inst.scale(0.1f, 0.1f, 0.1f);

	alpha_inst.mods = alpha_mod;
	alpha_inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(0.01, 0.01, 0.01));
	alpha_inst.skel = &Alpha_skel;
	//skeleton_inst.skel = &skely_skeleton;
	alpha_inst.mat = &Alpha_mat;
	//skeleton_inst.rotate(0,90.0f, 0);
	//skeleton_inst.translate(0, -5,-5);
	//{name = "RightHandPinky1" modelIndex = 2 boneIndex = 10 ...}
	alpha_inst.updateSkeleton();


	endpoint_inst2.mod = &endpoint_mod;
	endpoint_inst2.mat = &endpoint_mat;
	endpoint_inst2.scale(0.1, 0.1, 0.1);

	raft_inst.mod = &raft_mod;
	raft_inst.mat = &raft_mat;

	level_inst.mod = &level_mod;
	level_inst.mat = &level_mat;

	alpha_inst.skel->msm.setCurrentNode("injured_run");
	alpha_inst.skel->msm.initTransition("injured_run","falling_forward_death");
	//alpha_inst.skel->msm.initTransition("run", "run");
	
	skeleton_inst.skel->rootMotion.root_translation = glm::vec3(0, 0, 0.0f);
	skeleton_inst.skel->rootMotion.previous_translation = glm::vec3(0, 0, -100);


	glutMainLoop();

	return 0;
}

