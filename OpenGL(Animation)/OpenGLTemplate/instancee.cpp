#include "instancee.h"
#include "global.h"
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/string_cast.hpp> 
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>
#include "VBO.h"
#include <iostream>
#include <algorithm>

glm::mat4 quaternion2Matrix(glm::fquat q);

glm::mat4 instance::rotate(GLfloat rotX, GLfloat rotY, GLfloat rotZ)
{
	transform=glm::rotate(transform, rotX*0.0174533f, glm::vec3(1,0, 0));
	transform=glm::rotate(transform, rotY*0.0174533f, glm::vec3(0, 1, 0));
	transform=glm::rotate(transform, rotZ*0.0174533f, glm::vec3(0, 0, 1));
	return transform;
}

glm::mat4 instance::scale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ)
{
	transform = glm::scale(transform, glm::vec3(scaleX, scaleY, scaleZ));
	return transform;
}

glm::mat4 instance::setPosition(glm::vec3 position) {
	transform[3] = glm::vec4(position,transform[3][3]);
	return transform;
}

glm::mat4 instance::rotateAround(GLfloat rotX, GLfloat rotY, GLfloat rotZ, glm::vec3 pivot)
{
	//glm::mat4 transformation; // your transformation matrix.
	glm::vec3 scale;
	glm::quat rotation;
	glm::vec3 translation;
	glm::vec3 skew;
	glm::vec4 perspective;
	glm::decompose(transform, scale, rotation, translation, skew, perspective);
	//
	glm::vec3 offset = pivot - translation;
	transform=glm::translate(transform,offset);
	transform=glm::rotate(transform, rotY, glm::vec3(0, 1, 0));
	transform = glm::translate(transform,-offset);
	return transform;
}

glm::mat4 instance::translate(GLfloat transX, GLfloat transY, GLfloat transZ)
{
	transform=glm::translate(transform, glm::vec3(transX, transY, transZ));
	return transform;
}

void instance::updateBoneChildren(skeletonNode& node,glm::mat4& trans) {
	node.globalTransform = trans*node.localTransform;
	for (int i = 0; i < node.children.size(); i++) {
		updateBoneChildren(this->skel->nodes[node.children[i]],node.globalTransform);
	}
}

void instance::updateBone(skeletonNode& node, glm::mat4& trans) {
	node.localTransform = trans;
	if (node.parent != -1) {
		node.globalTransform = skel->nodes[node.parent].globalTransform*node.localTransform;
	}
	else {
		node.globalTransform = node.localTransform;
	}
	for (int i = 0; i < node.children.size(); i++) {
		updateBoneChildren(this->skel->nodes[node.children[i]],node.globalTransform);
	}
}

void instance::BindMaterial(){
	if (mat == NULL)return;
	glUseProgram(mat->ShaderID);
	mat->BindTextures();
	mat->BindValues();
	glm::mat4 MV = camera::ViewMatrix *transform;
	glm::mat3 MV3X3 = glm::mat3(MV);
	glm::mat4 MVP = camera::ProjectionMatrix * camera::ViewMatrix * transform;
	glm::mat4 VP = camera::ProjectionMatrix * camera::ViewMatrix;
	glm::mat4 MVPNoTranslation = camera::ProjectionMatrix*camera::ViewMatrixNoTranslation*transform;
	glm::mat4 NM = glm::transpose(glm::inverse(MV));
	time += time::deltaTime;
	GLuint MLocation = glGetUniformLocation(mat->ShaderID, "M");
	GLuint MVLocation = glGetUniformLocation(mat->ShaderID, "MV");
	GLuint MV3X3Location = glGetUniformLocation(mat->ShaderID, "MV3X3");
 	GLuint MVPLocation = glGetUniformLocation(mat->ShaderID, "MVP");
	GLuint NMLocation = glGetUniformLocation(mat->ShaderID, "NM");
	GLuint MVPNoTranslationLocation = glGetUniformLocation(mat->ShaderID, "MVPNoTranslation");
	GLuint TimerLocation = glGetUniformLocation(mat->ShaderID, "Timer");
	GLuint VPLocation = glGetUniformLocation(mat->ShaderID, "VP");

	//test
	//for (int i = 0; i < mod->boneOffsetMatricesContigious.size(); i++) {

	//}
	static float mkay = 0.0f;
	mkay += 0.1f;
	if (mod != NULL) {
		if (mod->boneOffsetMatricesContigious.size() > 0) {
			GLuint BoneMatrixLocation = glGetUniformLocation(mat->ShaderID, std::string("BoneMatrix").c_str());
			glUniformMatrix4fv(BoneMatrixLocation, animations::maxNumBones, GL_FALSE, &(mod->boneOffsetMatricesContigious[0])[0][0]);
		}
	}
	


	glUniformMatrix4fv(MLocation,1,GL_FALSE, &transform[0][0]);
	glUniformMatrix4fv(MVLocation, 1, GL_FALSE, &MV[0][0]);
	glUniformMatrix3fv(MV3X3Location, 1, GL_FALSE, &MV3X3[0][0]);
	glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(NMLocation, 1, GL_FALSE, &NM[0][0]);
	glUniformMatrix4fv(MVPNoTranslationLocation, 1, GL_FALSE, &MVPNoTranslation[0][0]);
	glUniformMatrix4fv(VPLocation, 1, GL_FALSE, &VP[0][0]);
	glUniform1f(TimerLocation, time);
	
}

void instance::BindModel()
{
	if (mod == NULL)return;
	buffers::Bind(*mod);
}

void instance::updateSkeleton(std::string nodeName)
{
	//find the node with the nodeName
	std::vector<skeletonNode>::iterator node= std::find_if(skel->nodes.begin(), skel->nodes.end(), [nodeName](auto i) {
		return i.name == nodeName;}
	);
	if (node == skel->nodes.end()) {
		nodeName = "arissa_Hips";
		node = std::find_if(skel->nodes.begin(), skel->nodes.end(), [nodeName](auto i) {
			return i.name == nodeName; }
		);
		if (node == skel->nodes.end()) {
			std::cout << "Could not find (root)node " << nodeName << std::endl;
			return;
		}
	}
	//apply the root motion to the model matrix
	glm::vec3 scale;
	glm::fquat rotation;
	glm::vec3 translation;
	glm::vec3 skew;
	glm::vec4 perspective;
	glm::decompose(node->localTransform, scale, rotation, translation, skew, perspective);
	rotation = glm::conjugate(rotation);
	rotation.x = 0;
	//rotation.z = 0;
	//double mag = sqrt(rotation.y * rotation.y + rotation.w * rotation.w);
	//rotation.y /= mag;
	//rotation.w /= mag;
	float RotX = glm::eulerAngles(rotation).x;
	float RotY = glm::eulerAngles(rotation).y;
	//float RotYq = RotY;
	float RotZ = glm::eulerAngles(rotation).z;
	//glm::vec3 right = glm::normalize(transform[0]);
	glm::vec3 up = glm::vec3(0, 1, 0);
	//glm::vec3 forward = glm::normalize(transform[2]);
	if (skel->newPlay) {
		skel->newPlay = false;
	}
	else {
		
		if (RotY < 0) {
			RotY += glm::pi<float>()*2.0f;
			//RotY += 180.0f;
		}
		//if (RotYq < 0) {
		//	//RotYq += 180.0f;//glm::pi<float>()*2.0f;
		//}
		//if (skel->msm.nodes[skel->msm.currentNode].name != "Standing_React_Death_Right") {
			skel->rootMotion.root_rotation = skel->rootMotion.root_rotation*rotation*glm::inverse(skel->rootMotion.previous_rotation);
			
			if (skel->msm.nodes[skel->msm.currentNode].name == "running_right_turn"
				|| skel->msm.nodes[skel->msm.currentNode].name == "running_left_turn") {
				//skel->rootMotion.root_RotY = glm::eulerAngles(skel->rootMotion.root_rotation).y;
				if (skel->msm.nodes[skel->msm.currentNode].name == "running_right_turn")
					skel->rootMotion.root_RotY -= time::deltaTime;
				else
					skel->rootMotion.root_RotY += time::deltaTime;
				skel->rootMotion.root_translation += 1.0f*(glm::vec3(glm::rotate(glm::mat4(1.0f), (skel->rootMotion.root_RotY), up)*glm::vec4(translation, 1.0f) - glm::rotate(glm::mat4(1.0f), (skel->rootMotion.root_RotY), up)*glm::vec4(skel->rootMotion.previous_translation, 1.0f)));
				//skel->rootMotion.root_RotYq += RotYq - skel->rootMotion.previous_RotYq;
				//skel->rootMotion.root_translation += glm::vec3(glm::toMat4(skel->rootMotion.root_rotation)*glm::vec4(translation, 1.0f) - glm::toMat4(skel->rootMotion.root_rotation)*glm::vec4(skel->rootMotion.previous_translation, 1.0f));
				//skel->rootMotion.root_RotY += RotY - skel->rootMotion.previous_RotY;

			}
			else {
				//skel->rootMotion.root_translation += glm::vec3(glm::toMat4(skel->rootMotion.root_rotation)*glm::vec4(translation, 1.0f) - glm::toMat4(skel->rootMotion.root_rotation)*glm::vec4(skel->rootMotion.previous_translation, 1.0f));
				skel->rootMotion.root_translation += glm::vec3(glm::rotate(glm::mat4(1.0f), (skel->rootMotion.root_RotY), up)*glm::vec4(translation, 1.0f) - glm::rotate(glm::mat4(1.0f), (skel->rootMotion.root_RotY), up)*glm::vec4(skel->rootMotion.previous_translation, 1.0f));
				//skel->rootMotion.root_translation += translation - skel->rootMotion.previous_translation;
			}
			skel->rootMotion.root_scale += scale - skel->rootMotion.previous_scale;

			if (skel->msm.nodes[skel->msm.currentNode].name != "running_jump"&&
				skel->msm.nodes[skel->msm.currentNode].name != "falling_forward_death"&&
				skel->msm.nodes[skel->msm.currentNode].name != "Standing_React_Death_Right") {
				skel->rootMotion.root_translation.y = skel->rootMotion.previous_translation.y;
				translation.y = skel->rootMotion.previous_translation.y;
			}
		//}
	}
	skel->rootMotion.previous_scale = scale;
	skel->rootMotion.previous_translation = translation;
	skel->rootMotion.previous_RotY = RotY;
	skel->rootMotion.previous_rotation = rotation;
	//skel->rootMotion.previous_RotYq = RotYq;
	glm::fquat addrotq= glm::angleAxis(skel->rootMotion.root_RotY,up);
	glm::fquat rotq = addrotq*rotation;
	transform = glm::translate(glm::mat4(1.0f), skel->rootMotion.root_translation)*
		//glm::toMat4(skel->rootMotion.root_rotation)*
		glm::toMat4(rotq)*
		glm::scale(glm::mat4(1.0f), skel->rootMotion.root_scale);
	transform = (glm::scale(glm::mat4(1.0f), glm::vec3(0.01, 0.01, 0.01)))*transform;
	glm::decompose(transform, scale, rotation, translation, skew, perspective);
	rotation = glm::conjugate(rotation);
	//set the viewmatrix to follow the player
	glm::mat4 rootMatrix = glm::inverse(glm::translate(glm::mat4(1.0f), translation)
		*glm::rotate(glm::mat4(1.0f),(skel->rootMotion.root_RotY),up)
		//*glm::toMat4(skel->rootMotion.root_rotation)
		*glm::scale(glm::mat4(1.0f),scale));
	glm::mat4 rootMatrixNoTrans = glm::rotate(glm::mat4(1.0f),(-skel->rootMotion.root_RotY), up)
		*glm::scale(glm::mat4(1.0f), scale);

	camera::ViewMatrix = glm::translate(glm::mat4(1.0f),glm::vec3(0,-100,-400.0f))*glm::rotate(glm::mat4(1.0f),glm::radians(180.0f),glm::vec3(0,1,0))*rootMatrix;
	camera::ViewMatrixNoTranslation = rootMatrixNoTrans;
	//update the current node local transform
	node->globalTransform = node->localTransform;
	//if this is the player, update the View matrix
	static float scale_f = 1.0f;
	scale_f += -0.001f;
	
	updateSkeletonRec(*node);
}

void instance::updateSkeletonRec(skeletonNode & node)
{
	if (node.modelBoneIndices.size()>0) {
		for (int i = 0; i < node.modelBoneIndices.size(); i++) {
			glm::vec4 bef = mods[node.modelBoneIndices[i].first].boneOffsetMatricesContigious[node.modelBoneIndices[i].second][3];
			mods[node.modelBoneIndices[i].first].boneOffsetMatricesContigious[node.modelBoneIndices[i].second] =
				glm::inverse(skel->nodes[skel->hips].globalTransform) *
				node.globalTransform*mods[node.modelBoneIndices[i].first].bones[node.modelBoneIndices[i].second].offsetTransform;
			glm::vec4 aft = mods[node.modelBoneIndices[i].first].boneOffsetMatricesContigious[node.modelBoneIndices[i].second][3];
			if (node.name != mods[node.modelBoneIndices[i].first].bones[node.modelBoneIndices[i].second].name) {
				exit(-777);
			}
		}
	}
	//update the global transform of all children with the global transform of the node
	for (int i = 0; i < node.children.size(); i++) {
		skel->nodes[node.children[i]].globalTransform = node.globalTransform*skel->nodes[node.children[i]].localTransform;
		//call the rec function to the child
		updateSkeletonRec(skel->nodes[node.children[i]]);
	}
}

void instance::UpdateNode(std::string nodeName,glm::mat4 trans)
{
	std::vector<skeletonNode>::iterator node = std::find_if(skel->nodes.begin(), skel->nodes.end(), [nodeName](auto i) {
		return i.name == nodeName; }
	);
	if (node == skel->nodes.end()) {
		std::cout << "Could not find node " <<nodeName<< std::endl;
		return;
	}
	std::cout << nodeName << std::endl;
	//update the current node local transform
	node->localTransform = trans;
	updateSkeleton();
}

void instance::UpdateAnimation() {
	int animation_index1 = skel->msm.previousNode;
	int animation_index2 = skel->msm.currentNode;
	if (previousAnimationIndex != animation_index2) {
		timer1 = timer2;
		timer2 = 0.0f;
		skel->newPlay = true;
	}
	previousAnimationIndex = animation_index2;
	int trans_index1 = 0;
	int trans_index2 = 1;
	int trans_index3 = 0;
	int trans_index4 = 1;
	float alpha1 = 0.0f;
	float alpha2 = 0.0f;
	timer1 += time::deltaTime;
	timer2 += time::deltaTime;
	
	//find the node in the outcoming
	std::vector<int>::iterator it = std::find_if(skel->msm.nodes[skel->msm.previousNode].outcoming.begin(), skel->msm.nodes[skel->msm.previousNode].outcoming.end(),
		[animation_index2](auto i) {return i == animation_index2; }
	);
	int index = std::distance(skel->msm.nodes[skel->msm.previousNode].outcoming.begin(), it);
	if (it == skel->msm.nodes[skel->msm.previousNode].outcoming.end()) {
		//no outcoming state
		skel->msm.transitionPercentage = 1.0f;
	}
	else {
		//calculate the percentage
		skel->msm.transitionPercentage = timer2 / skel->msm.nodes[skel->msm.previousNode].out_time[index];
		skel->msm.transitionPercentage = glm::clamp(skel->msm.transitionPercentage, 0.0f, 1.0f);
	}
	float tickTime1 = timer1*skel->animations[animation_index1].ticksPerSecond;
	float animTime1 = fmod(tickTime1, skel->animations[animation_index1].duration);
	float tickTime2 = timer2*skel->animations[animation_index2].ticksPerSecond;
	float animTime2 = fmod(tickTime2, skel->animations[animation_index2].duration);
	animations::currentAnimationDuration = skel->animations[animation_index2].duration;
	if (animTime2 < previousAnimTime) {
		skel->newPlay = true;
	}
	previousAnimTime = animTime2;
	//find the two indices for the translation using the animation time
	//first animation
	for (int i = 0; i < skel->animations[animation_index1].numKeyFrames-1; i++) {
		trans_index1 = i;
		trans_index2 = i + 1;
		if (animTime1 < skel->animations[animation_index1].nodes[skel->animations[animation_index1].activeNodeIndex].translations[i + 1].second) {
			float total = skel->animations[animation_index1].nodes[skel->animations[animation_index1].activeNodeIndex].translations[i + 1].second -
				skel->animations[animation_index1].nodes[skel->animations[animation_index1].activeNodeIndex].translations[i].second;
			alpha1 = (animTime1 - skel->animations[animation_index1].nodes[skel->animations[animation_index1].activeNodeIndex].translations[i].second) / total;
			alpha1 = glm::clamp(alpha1, 0.0f, 1.0f);
			break;
		}
	}
	//second animation
	for (int i = 0; i < skel->animations[animation_index2].numKeyFrames-1; i++) {
		trans_index3 = i;
		trans_index4 = i + 1;
		if (animTime2 < skel->animations[animation_index2].nodes[skel->animations[animation_index2].activeNodeIndex].translations[i + 1].second) {
			float total = skel->animations[animation_index2].nodes[skel->animations[animation_index2].activeNodeIndex].translations[i + 1].second -
				skel->animations[animation_index2].nodes[skel->animations[animation_index2].activeNodeIndex].translations[i].second;
			alpha2 = (animTime2 - skel->animations[animation_index2].nodes[skel->animations[animation_index2].activeNodeIndex].translations[i].second) / total;	
			alpha2 = glm::clamp(alpha2, 0.0f, 1.0f);
			break;
		}
	}

	for (int i = 0; i < skel->animations[animation_index2].nodes.size(); i++) {
		if (skel->animations[animation_index2].nodes[i].name == "") {
			continue;
		}
		
		glm::vec3 trans1 =
			glm::lerp(skel->animations[animation_index1].nodes[i].translations[trans_index1].first, skel->animations[animation_index1].nodes[i].translations[trans_index2].first, alpha1);
		glm::fquat rot1  =
			glm::slerp(skel->animations[animation_index1].nodes[i].rotations[trans_index1].first, skel->animations[animation_index1].nodes[i].rotations[trans_index2].first, alpha1);
		glm::vec3 scale1 =
			glm::lerp(skel->animations[animation_index1].nodes[i].scalings[trans_index1].first, skel->animations[animation_index1].nodes[i].scalings[trans_index2].first, alpha1);

		glm::vec3 trans2 =
			glm::lerp(skel->animations[animation_index2].nodes[i].translations[trans_index3].first, skel->animations[animation_index2].nodes[i].translations[trans_index4].first, alpha2);
		glm::fquat rot2 =
			glm::slerp(skel->animations[animation_index2].nodes[i].rotations[trans_index3].first, skel->animations[animation_index2].nodes[i].rotations[trans_index4].first, alpha2);
		glm::vec3 scale2 =
			glm::lerp(skel->animations[animation_index2].nodes[i].scalings[trans_index3].first, skel->animations[animation_index2].nodes[i].scalings[trans_index4].first, alpha2);

		//blend the tranformations
		if (skel->animations[animation_index2].nodes[i].name == "Hips"|| skel->animations[animation_index2].nodes[i].name == "arissa_Hips") {
			trans1 = trans2;
			scale1 = scale2;
		}
		glm::vec3 trans = glm::lerp(trans1, trans2, skel->msm.transitionPercentage);
		glm::fquat rot = glm::slerp(rot1, rot2, skel->msm.transitionPercentage);
		glm::vec3 scale = glm::lerp(scale1, scale2, skel->msm.transitionPercentage);

		//if (skel->animations[animation_index2].nodes[i].name == "Hips") {
		//	//if (skel->animations[animation_index2].name == "jump") {
		//	trans = glm::vec3(1000,1000, 1000);
		//	//}
		//}

		glm::mat4 translation = glm::translate(glm::mat4(1.0f), trans);
		glm::mat4 rotation = glm::toMat4(rot);
		glm::mat4 scaling = glm::scale(glm::mat4(1.0f), scale);
		skel->nodes[i].localTransform = translation*rotation*scaling;
	}
	updateSkeleton();
}

void instance::UpdateAnimation(std::string animationName)
{
	//find the animation index using the string
	std::vector<animation>::iterator result= std::find_if(skel->animations.begin(), skel->animations.end(), [animationName](auto elem) {
		return elem.name == animationName;
	});
	int animation_index = std::distance(skel->animations.begin(), result);
	
	static float timer=0.0f;
	if (animationName != previousAnimationName) {
		timer = 0.0f;
	}
	previousAnimationName = animationName;
	int trans_index1 = 0;
	int trans_index2 = 1;
	float alpha = 0.0f;
	timer += time::deltaTime;
	float tickTime = timer*skel->animations[animation_index].ticksPerSecond;
	float animTime = fmod(tickTime, skel->animations[animation_index].duration);
	//find the two indices for the translation using the animation time
	for (int i = 0; i < skel->animations[animation_index].numKeyFrames;i++) {//@TODO remove the 2
		trans_index1 = i;
		trans_index2 = i + 1;
		if (animTime < skel->animations[animation_index].nodes[skel->animations[animation_index].activeNodeIndex].translations[i + 1].second) {
			float total = skel->animations[animation_index].nodes[skel->animations[animation_index].activeNodeIndex].translations[i + 1].second -
				skel->animations[animation_index].nodes[skel->animations[animation_index].activeNodeIndex].translations[i].second;
			alpha = (animTime - skel->animations[animation_index].nodes[skel->animations[animation_index].activeNodeIndex].translations[i].second)/total;
			break;
		}	
	}
	for (int i = 0; i < skel->animations[animation_index].nodes.size(); i++) {
		if (skel->animations[animation_index].nodes[i].name == "") {
			continue;
		}
		glm::vec3 trans = 
			glm::lerp(skel->animations[animation_index].nodes[i].translations[trans_index1].first, skel->animations[animation_index].nodes[i].translations[trans_index2].first, alpha);
		glm::fquat rot = 
			glm::lerp(skel->animations[animation_index].nodes[i].rotations[trans_index1].first, skel->animations[animation_index].nodes[i].rotations[trans_index2].first, alpha);
		glm::vec3 scale = 
			glm::lerp(skel->animations[animation_index].nodes[i].scalings[trans_index1].first, skel->animations[animation_index].nodes[i].scalings[trans_index2].first, alpha);

		glm::mat4 translation = glm::translate(glm::mat4(1.0f),trans);
		glm::mat4 rotation = glm::toMat4(rot);
		glm::mat4 scaling = glm::scale(glm::mat4(1.0f), scale);
		skel->nodes[i].localTransform = translation*rotation*scaling;
	}
	updateSkeleton();
}

void instance::Render()
{
	if (mods.size() > 0) {
		for (int i = 0; i <mods.size(); i++) {
			mod = &mods[i];
			if(i<mats.size())
				mat = &mats[i];
			BindModel();
			BindMaterial();
			buffers::RenderTriangles(mod->vertices.size());
		}
	}
	else {
		BindMaterial();
		BindModel();
		buffers::RenderTriangles(mod->vertices.size());
	}
}



void instance::updateIKCCD3D(skeletonNode& node,skeletonNode& root,glm::vec3 e) {
	//get the scale of the transformation
	//CCD
	float epsilon = 0.1;
	int max_it = 1000;
	int it_counter = 0;
	
	//get the bone that corresponds to this node (could be many..)
	bone& endbono = mods[node.modelBoneIndices[0].first].bones[node.modelBoneIndices[0].second];
	glm::mat4 endOffsetMatrix = mods[node.modelBoneIndices[0].first].boneOffsetMatricesContigious[node.modelBoneIndices[0].second];
	//while the distance between the endpoint and the node is less than the threshold
	glm::vec3 end = transform*endOffsetMatrix*glm::vec4(endbono.head,1.0f);
	float length = 0;
	skeletonNode* temp = &node;
	while (temp->name!=root.name) {
		length +=
			glm::length(transform*glm::vec4(mods[temp->modelBoneIndices[0].first].bones[temp->modelBoneIndices[0].second].bbox.axisA, 0.0f))
			*mods[temp->modelBoneIndices[0].first].bones[temp->modelBoneIndices[0].second].length;
		temp = &skel->nodes[temp->parent];
	}
	bone& startbono = mods[root.modelBoneIndices[0].first].bones[root.modelBoneIndices[0].second];
	glm::mat4 startOffsetMatrix = mods[root.modelBoneIndices[0].first].boneOffsetMatricesContigious[root.modelBoneIndices[0].second];
	glm::vec3 start = transform*startOffsetMatrix*glm::vec4(startbono.tail, 1.0f);
	
	if (length < glm::length(start - e)) {;
		return;
	}
	while (glm::distance(e, end) > epsilon&&it_counter++ < max_it) {
		//get the node and traverse up to the root
		skeletonNode* temp = &skel->nodes[node.parent];
		while (temp->name != root.name) {
			//get the bone of the node
			bone& bono = mods[temp->modelBoneIndices[0].first].bones[temp->modelBoneIndices[0].second];
			glm::mat4 offMatrix = mods[temp->modelBoneIndices[0].first].boneOffsetMatricesContigious[temp->modelBoneIndices[0].second];
			//create the pivot vector
			glm::vec3 pivot = transform*offMatrix*glm::vec4(bono.tail, 1.0f);
			glm::vec3 target = e;
			glm::vec3 endpoint = transform*offMatrix*glm::vec4(endbono.tail, 1.0f);
			
			glm::vec3 t_vec = (target - pivot);
			glm::vec3 e_vec = (endpoint - pivot);

			//get the angle
			float cosa = (glm::dot(e_vec, t_vec)) / (glm::length(t_vec)*glm::length(e_vec));
			glm::vec3 axis = glm::normalize(glm::cross(e_vec, t_vec));
			float anglea = glm::acos(glm::clamp(cosa, -1.0f, 1.0f));
			//rotate the link
			temp->localTransform = glm::translate(temp->localTransform, glm::vec3(transform*glm::vec4(bono.tail,1.0f)));
			temp->localTransform = glm::rotate(temp->localTransform, 0.01f*anglea, axis);
			temp->localTransform = glm::translate(temp->localTransform, -glm::vec3(transform*glm::vec4(bono.tail, 1.0f)));
			
			

			this->updateBone(*temp, temp->localTransform);
			updateSkeleton();
			//continue until you reach the father
			temp = &skel->nodes[temp->parent];
		}
		temp = &skel->nodes[node.parent];
		glm::mat4 endOffsetMatrix = mods[node.modelBoneIndices[0].first].boneOffsetMatricesContigious[node.modelBoneIndices[0].second];
		end = transform*endOffsetMatrix*glm::vec4(endbono.head, 1.0f);
	}
	updateSkeleton();
}

void instance::RenderWithoutMaterials() {
	buffers::Bind(mod->verticesID);
	glUseProgram(mat->ShaderID);
	glm::mat4 MVP = camera::ProjectionMatrix * camera::ViewMatrix * transform;
	GLuint MVPLocation = glGetUniformLocation(mat->ShaderID, "MVP");
	glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, &MVP[0][0]);

	GLuint MLocation = glGetUniformLocation(mat->ShaderID, "M");
	glUniformMatrix4fv(MLocation, 1, GL_FALSE, &transform[0][0]);
	//GLuint colorLocation = glGetUniformLocation(mat->ShaderID, "color");
	
	//glUniform3f(colorLocation,mat->color.r,mat->color.g, mat->color.b);
	buffers::RenderTriangles(mod->vertices.size());
}

void instance::RotateEuler() {
	//create the Rx, Ry, Rz matrices
	float x = eulerAngles.x;
	float y = eulerAngles.y;
	float z = eulerAngles.z;

	glm::mat4 Rx = 
		glm::mat4(
		glm::vec4(1, 0, 0, 0),
		glm::vec4(0, glm::cos(x), -glm::sin(x), 0),
		glm::vec4(0, glm::sin(x), glm::cos(x), 0),
		glm::vec4(0, 0, 0, 1)
	);

	glm::mat4 Ry = 
			glm::mat4(
		glm::vec4(glm::cos(y), 0, glm::sin(y), 0),
		glm::vec4(0, 1,0, 0),
		glm::vec4(-glm::sin(y), 0, glm::cos(y), 0),
		glm::vec4(0, 0, 0, 1)
		);

	glm::mat4 Rz = 
		glm::mat4(
		glm::vec4(glm::cos(z), -glm::sin(z), 0, 0),
		glm::vec4(glm::sin(z), glm::cos(z), 0, 0),
		glm::vec4(0,0, 1, 0),
		glm::vec4(0, 0, 0, 1)
		);
	//create the transform as the proudct of the matrices
	transform = glm::translate(glm::mat4(1.0),this->position)*Rx*Ry*Rz;
}


glm::mat4 quaternion2Matrix(glm::fquat q){
	return glm::mat4(
		glm::vec4(1 - 2 * q.z*q.z - 2 * q.w*q.w, 2 * q.y*q.z - 2 * q.x*q.w, 2 * q.x*q.z + 2 * q.y*q.w, 0),
		glm::vec4(2 * q.y*q.z + 2 * q.x*q.w, 1 - 2 * q.y*q.y - 2 * q.w*q.w, 2 * q.z*q.w - 2 * q.x*q.y, 0),
		glm::vec4(2 * q.y*q.w - 2 * q.x*q.z, 2 * q.x*q.y + 2 * q.z*q.w, 1 - 2 * q.y*q.y - 2 * q.z*q.z, 0),
		glm::vec4(glm::vec3(0.0f), 1)
	);
}

glm::vec4 multiplyQuaternions(glm::vec4 p, glm::vec4 q) {
	float qs = q[0];
	float ps = p[0];
	glm::vec3 pv = glm::vec3(p[1], p[2], p[3]);
	glm::vec3 qv = glm::vec3(q[1], q[2], q[3]);
	float pqs = ps*qs- glm::dot(pv,qv);
	glm::vec3 pqv = ps*qv+qs*pv+glm::cross(pv,qv);
	glm::vec4 result = glm::vec4(pqs,pqv);
	return glm::normalize(result);
}

void instance::Yaw(float angle) {
	//angle = 0.5f;
	forward = -transform[2];
	right = transform[0];
	up = transform[1];
	glm::vec4 qy = glm::vec4(glm::cos(angle*0.5f),up*glm::sin(angle*0.5f));
	q = multiplyQuaternions(q,qy);
	glm::mat4 rotate=glm::mat4(
		glm::vec4(1 - 2 * q.z*q.z - 2 * q.w*q.w, 2 * q.y*q.z - 2 * q.x*q.w, 2 * q.x*q.z + 2 * q.y*q.w, 0),
		glm::vec4(2 * q.y*q.z + 2 * q.x*q.w, 1 - 2 * q.y*q.y - 2 * q.w*q.w, 2 * q.z*q.w - 2 * q.x*q.y, 0),
		glm::vec4(2 * q.y*q.w - 2 * q.x*q.z, 2 * q.x*q.y + 2 * q.z*q.w, 1 - 2 * q.y*q.y - 2 * q.z*q.z, 0),
		glm::vec4(glm::vec3(this->position), 1)
	);
	
}
void instance::Pitch(float angle) {
	forward = transform[2];
	right = transform[0];
	up = transform[1];
	glm::vec4 qx = glm::vec4(glm::cos(angle*0.5f), right*glm::sin(angle*0.5f));
	q = multiplyQuaternions(q, qx);
	transform = glm::mat4(
		glm::vec4(1 - 2 * q.z*q.z - 2 * q.w*q.w, 2 * q.y*q.z - 2 * q.x*q.w, 2 * q.x*q.z + 2 * q.y*q.w, 0),
		glm::vec4(2 * q.y*q.z + 2 * q.x*q.w, 1 - 2 * q.y*q.y - 2 * q.w*q.w, 2 * q.z*q.w - 2 * q.x*q.y, 0),
		glm::vec4(2 * q.y*q.w - 2 * q.x*q.z, 2 * q.x*q.y + 2 * q.z*q.w, 1 - 2 * q.y*q.y - 2 * q.z*q.z, 0),
		glm::vec4(glm::vec3(this->position), 1)
	);
}
void instance::Roll(float angle) {
	forward = transform[2];
	right = transform[0];
	up = transform[1];
	glm::vec4 qz = glm::vec4(glm::cos(angle*0.5f), forward*glm::sin(angle*0.5f));
	q = multiplyQuaternions(q, qz);
	transform = glm::mat4(
		glm::vec4(1 - 2 * q.z*q.z - 2 * q.w*q.w, 2 * q.y*q.z - 2 * q.x*q.w, 2 * q.x*q.z + 2 * q.y*q.w, 0),
		glm::vec4(2 * q.y*q.z + 2 * q.x*q.w, 1 - 2 * q.y*q.y - 2 * q.w*q.w, 2 * q.z*q.w - 2 * q.x*q.y, 0),
		glm::vec4(2 * q.y*q.w - 2 * q.x*q.z, 2 * q.x*q.y + 2 * q.z*q.w, 1 - 2 * q.y*q.y - 2 * q.z*q.z, 0),
		glm::vec4(glm::vec3(this->position), 1)
	);
}
