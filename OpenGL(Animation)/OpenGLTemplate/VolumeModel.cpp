#include "VolumeModel.h"
#include "debug.h"
#include <glm\gtx\string_cast.hpp>
#include <iostream>

#define G -9.8f //Gravity acceleration
#define rho 1.0f //fluid density
#define p0 1.01f*10.0f //atmospheric pressure in the system
#define l 1.0f //length of pipe
#define c 1.0f //cross sectional area of the pipe
#define deltaTime 0.01f //fixed delta time

void VolumeModel::printVolumes() {
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			std::cout << columns[i][j].volume << "|";
		}
		std::cout << std::endl;
	}
}

VolumeModel::VolumeModel(instance inst,int dimension,float dx,float dy) {
	columns.resize(dimension);
	for (int i = 0; i < dimension; i++) {
		columns[i].resize(dimension);
	}
	for(int i=0;i<dimension;i++){
		for (int j = 0; j < dimension; j++) {
			columns[i][j].inst = instance(inst);
			columns[i][j].volume = 1.0f;  //+i*0.1f+j*0.1f;
			totalVolume += columns[i][j].volume*0.25f;
			//orthogonal
			if (i % 2 == j % 2) {
				if (i > 0) {
					pipes.push_back(Pipe(glm::ivec2(i - 1, j), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j), glm::ivec2(i - 1, j)));
				}
				if (i < dimension - 1) {
					pipes.push_back(Pipe(glm::ivec2(i + 1, j), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i + 1, j)));
				}
				if (j > 0) {
					pipes.push_back(Pipe(glm::ivec2(i, j - 1), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i, j - 1)));
				}
				if (j < dimension - 1) {
					pipes.push_back(Pipe(glm::ivec2(i, j + 1), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i, j + 1)));
				}
			}
			//cross
			if (i % 2 == 0) {
				if (i > 0 && j > 0) {
					pipes.push_back(Pipe(glm::ivec2(i - 1, j - 1), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i - 1, j - 1)));
				}
				if (i > 0 && j < dimension - 1) {
					pipes.push_back(Pipe(glm::ivec2(i - 1, j + 1), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i - 1, j + 1)));
				}
				if (i < dimension - 1 && j>0) {
					pipes.push_back(Pipe(glm::ivec2(i + 1, j - 1), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i + 1, j - 1)));
				}
				if (i < dimension - 1 && j < dimension - 1) {
					pipes.push_back(Pipe(glm::ivec2(i + 1, j + 1), glm::ivec2(i, j)));
					pipes.push_back(Pipe(glm::ivec2(i, j),glm::ivec2(i + 1, j + 1)));
				}
			}
		}
	}
	//////add draining pipes on the frame (ortho)
	for (int i = 0; i < dimension; i++) {
		pipes.push_back(Pipe(glm::ivec2(i, 0), glm::ivec2(-1, -1)));
		pipes.push_back(Pipe(glm::ivec2(i, dimension - 1), glm::ivec2(-1, -1)));
	}
	for (int j = 0; j < dimension; j++) {
		pipes.push_back(Pipe(glm::ivec2(0, j), glm::ivec2(-1, -1)));
		pipes.push_back(Pipe(glm::ivec2(dimension-1, j), glm::ivec2(-1, -1)));
	}
	this->dx = dx;
	this->dy = dy;
	this->dimension = dimension;
}

float VolumeModel::calculatePipeAcceleration(glm::ivec2 col1, glm::ivec2 col2)
{
	
	float pressure1 = columns[col1.x][col1.y].calculateTotalPresure(dx, dy);
	float pressure2 = 0.0f; 
	if(col2.x>-1&&col2.y>-1)
		pressure2=columns[col2.x][col2.y].calculateTotalPresure(dx, dy);
	return ((pressure1 - pressure2)) / (rho*l);
}

float VolumeModel::calculatePipeFlowDifferential(glm::ivec2 col1, glm::ivec2 col2)
{
	
	float pipeAcceleration = calculatePipeAcceleration(col1, col2);
	if (col2.x == -1 && col2.y == -1) pipeAcceleration = glm::abs(pipeAcceleration)*-1.0f;
	return pipeAcceleration*deltaTime*c;
}

float VolumeModel::calculateColumnVerticalVelocity(glm::ivec2 col1)
{
	float netFlow = 0.0f;
	if (col1.x > 0) {
		netFlow += columns[col1.x-1][col1.y].volumeDifferential;
	}
	if (col1.x < dimension - 1) {
		netFlow += columns[col1.x + 1][col1.y].volumeDifferential;
	}
	if (col1.y > 0) {
		netFlow += columns[col1.x][col1.y - 1].volumeDifferential;
	}
	if (col1.y < dimension - 1) {
		netFlow += columns[col1.x][col1.y + 1].volumeDifferential;
	}
	if (col1.x > 0 && col1.y > 0) {
		netFlow += columns[col1.x - 1][col1.y-1].volumeDifferential;
	}
	if (col1.x < dimension - 1 && col1.y < dimension - 1) {
		netFlow += columns[col1.x + 1][col1.y + 1].volumeDifferential;
	}
	if (col1.x > 0 && col1.y < dimension - 1) {
		netFlow += columns[col1.x - 1][col1.y + 1].volumeDifferential;
	}
	if (col1.x < dimension - 1 && col1.y>0) {
		netFlow += columns[col1.x + 1][col1.y - 1].volumeDifferential;
	}
	return netFlow / (dx*dy);
}

glm::vec2 VolumeModel::calculateColumnHorizontalVelocity(glm::ivec2 col1) {
	float horizontalFlowX = 0;
	float horizontalFlowZ = 0;
	if (col1.x < dimension - 1 && col1.y<dimension-1) {
		horizontalFlowX = (columns[col1.x + 1][col1.y].volumeDifferential + columns[col1.x + 1][col1.y + 1].volumeDifferential)*0.5f;
		horizontalFlowZ = (columns[col1.x][col1.y + 1].volumeDifferential + columns[col1.x + 1][col1.y + 1].volumeDifferential)*0.5f;
	}
	return glm::vec2(horizontalFlowX, horizontalFlowZ);
}

void VolumeModel::updatePipeFlows() {
	for (int i = 0; i < pipes.size(); i++) {
			pipes[i].flowDifferential = calculatePipeFlowDifferential(pipes[i].col1, pipes[i].col2);
	}
	//std::cout << "flow dif iz" << pipes[dimension - 1].flowDifferential << std::endl;
}

void VolumeModel::updateColumnVolumes() {
	float current_volume = 0.0f;
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			columns[i][j].volumeDifferential = 0.0f;
		}
	}
	updatePipeFlows();
	for (int i = 0; i < pipes.size(); i++) {
		columns[pipes[i].col1.x][pipes[i].col1.y].volumeDifferential += (pipes[i].flowDifferential + pipes[i].flow)*0.5f*deltaTime;
	}
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			columns[i][j].volume += columns[i][j].volumeDifferential;
			if (columns[i][j].volume < 0) {
				
				//std::cout << "negative vol" << columns[i][j].volume<< std::endl;
				columns[i][j].volume = 0.0f;
			}
			current_volume += columns[i][j].volume;
		}
	}
	//std::cout << "total vol is " << current_volume << std::endl;
	//check if there is a volume gap and restore the difference uniformily
	if (current_volume < totalVolume) {
		//std::cout << current_volume << " vs " << totalVolume << std::endl;
		float volume_diff = (totalVolume - current_volume) / (dimension*dimension);
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				columns[i][j].volume += volume_diff;
			}
		}
	}
	for (int i = 0; i < pipes.size(); i++) {
		pipes[i].flow += pipes[i].flowDifferential;
	}
}

void VolumeModel::Render()
{
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			columns[i][j].Render(i,j,dx,dy);
		}
	}
}

float Column::calculateHeight(float dx,float dy) {
	return volume / (dx*dy);
}

float Column::calculateStaticPresure(float dx, float dy) {
	float height = calculateHeight(dx,dy);
	return height*G*rho+p0;
}

float Column::calculateTotalPresure(float dx, float dy) {
	float staticPressure = calculateStaticPresure(dx,dy)+externalPressure;
	return staticPressure;
}



void Column::Render(int i,int j,float dx, float dy)
{
	//set the scale matrix according to height and dx and dy
	float height = calculateHeight(dx, dy);
	inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(i*dx, height, j*dy));
	inst.transform = glm::scale(inst.transform, glm::vec3(dx*0.5f, height, dy*0.5f));
	inst.Render();
}

Pipe::Pipe(glm::ivec2 col1, glm::ivec2 col2) {
	this->col1 = col1;
	this->col2 = col2;
}
