#include "AuxilaryIO.h"
#include "debug.h"
#include "bone.h"
#include "global.h"
//stl
#include <vector>
#include <deque>
#include<iostream>
#include <algorithm>
#include <fstream>
//glm
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
//assimp
#include <assimp/postprocess.h>     // Post processing flags
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

bool load(const char * path, std::vector <glm::vec3> & out_vertices, std::vector <glm::vec3> & out_normals, std::vector <glm::vec2> & out_uvs)
{
	// Create an instance of the Importer class

	Assimp::Importer importer;
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll 
	// propably to request more postprocessing than we do in this example.
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_Triangulate
		//|aiProcess_JoinIdenticalVertices
	);
	if (!scene)
	{

		std::string errorString = importer.GetErrorString();
		printConsole(errorString);
		return false;
	}
	else {
		size_t size_acc = 0;
		for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
			size_acc += scene->mMeshes[j]->mNumVertices;
		}
		out_vertices.reserve(size_acc);
		out_normals.reserve(size_acc);
		out_uvs.reserve(size_acc);
		for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
			aiMesh* mesh = scene->mMeshes[j];
			for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
				out_vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[0]].x, mesh->mVertices[mesh->mFaces[i].mIndices[0]].y, mesh->mVertices[mesh->mFaces[i].mIndices[0]].z));
				out_vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[1]].x, mesh->mVertices[mesh->mFaces[i].mIndices[1]].y, mesh->mVertices[mesh->mFaces[i].mIndices[1]].z));
				out_vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[2]].x, mesh->mVertices[mesh->mFaces[i].mIndices[2]].y, mesh->mVertices[mesh->mFaces[i].mIndices[2]].z));

				out_normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[0]].x, mesh->mNormals[mesh->mFaces[i].mIndices[0]].y, mesh->mNormals[mesh->mFaces[i].mIndices[0]].z));
				out_normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[1]].x, mesh->mNormals[mesh->mFaces[i].mIndices[1]].y, mesh->mNormals[mesh->mFaces[i].mIndices[1]].z));
				out_normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[2]].x, mesh->mNormals[mesh->mFaces[i].mIndices[2]].y, mesh->mNormals[mesh->mFaces[i].mIndices[2]].z));

				out_uvs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].z));
				out_uvs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].z));
				out_uvs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].z));

			}
		}
	}
	return true;
}

glm::mat4 convertAiMat2GlmMat(aiMatrix4x4 mat) {
	glm::mat4 nmat;
	
	nmat = glm::mat4(mat.a1, mat.a2, mat.a3, mat.a4,
		mat.b1, mat.b2, mat.b3, mat.b4,
		mat.c1, mat.c2, mat.c3, mat.c4,
		mat.d1, mat.d2, mat.d3, mat.d4
	);
	nmat = glm::transpose(nmat);
	//std::cout << "nmat[3] is " << glm::to_string(nmat[3]) << std::endl;
	return nmat;
}

glm::vec3 convertAiV32GLMV3(aiVector3D vec) {
	return glm::vec3(vec.x, vec.y, vec.z);
}

glm::fquat convertAiQuart2GLMQuart(aiQuaternion quart) {
	return glm::fquat(quart.w, quart.x, quart.y, quart.z);
}

void CalculateBoneMetaInfo(std::vector<model>& mods) {
	//calculates AABB and length infomation for the bone. This is necessary for IK and collision detection with OBBs
	//iterate over each bone
	for (int mod_c = 0; mod_c < mods.size(); mod_c++) {
		model& mod = mods[mod_c];
		for (int i = 0; i < mod.bones.size(); i++) {
			//find the minmax of x y z between the vertices that are affected by this bone
			glm::vec3 min = glm::vec3(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
			glm::vec3 max = -min;
			for (int j = 0; j < mod.boneIndicesContigious.size(); j++) {
				float index = mod.boneIndicesContigious[j][0];
				if (index != i)
					continue;
				glm::vec3 vertex = mod.vertices[j];
				if (vertex.x > max.x) {
					max.x = vertex.x;
				}
				if (vertex.y > max.y) {
					max.y = vertex.y;
				}
				if (vertex.z > max.z) {
					max.z = vertex.z;
				}
				if (vertex.x < min.x) {
					min.x = vertex.x;
				}
				if (vertex.y < min.y) {
					min.y = vertex.y;
				}
				if (vertex.z < min.z) {
					min.z = vertex.z;
				}
			}
			//calucalte the AABB from min max
			mod.bones[i].bbox = AABB(min, max);
			//calculate the length from AABB
			mod.bones[i].length = glm::length(mod.bones[i].bbox.size*mod.bones[i].bbox.axisA);
			//calucate the head and tail of the bone
			mod.bones[i].head = mod.bones[i].bbox.center + (mod.bones[i].bbox.size*mod.bones[i].bbox.axisA)*0.5f;
			mod.bones[i].tail = mod.bones[i].bbox.center - (mod.bones[i].bbox.size*mod.bones[i].bbox.axisA)*0.5f;
		}
	}
}

bool load(const char* path,std::vector<model>& mod, std::vector<material>& mats) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_Triangulate
		//|aiProcess_JoinIdenticalVertices
	);
	if (!scene)
	{
		std::string errorString = importer.GetErrorString();
		printConsole(errorString);
		return false;
	}
	//harvest vertices, normals and UVs
	for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
		aiMesh* mesh = scene->mMeshes[j];
		mod.push_back(model());
		mats.push_back(material());
		mod[j].vertices.reserve(scene->mMeshes[j]->mNumFaces * 3);
		mod[j].normals.reserve(scene->mMeshes[j]->mNumFaces * 3);
		mod[j].UVs.reserve(scene->mMeshes[j]->mNumFaces * 3);
		mod[j].name = mesh->mName.C_Str();
		for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
			mod[j].vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[0]].x, mesh->mVertices[mesh->mFaces[i].mIndices[0]].y, mesh->mVertices[mesh->mFaces[i].mIndices[0]].z));
			mod[j].vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[1]].x, mesh->mVertices[mesh->mFaces[i].mIndices[1]].y, mesh->mVertices[mesh->mFaces[i].mIndices[1]].z));
			mod[j].vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[2]].x, mesh->mVertices[mesh->mFaces[i].mIndices[2]].y, mesh->mVertices[mesh->mFaces[i].mIndices[2]].z));

			mod[j].normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[0]].x, mesh->mNormals[mesh->mFaces[i].mIndices[0]].y, mesh->mNormals[mesh->mFaces[i].mIndices[0]].z));
			mod[j].normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[1]].x, mesh->mNormals[mesh->mFaces[i].mIndices[1]].y, mesh->mNormals[mesh->mFaces[i].mIndices[1]].z));
			mod[j].normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[2]].x, mesh->mNormals[mesh->mFaces[i].mIndices[2]].y, mesh->mNormals[mesh->mFaces[i].mIndices[2]].z));

			mod[j].UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].z));
			mod[j].UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].z));
			mod[j].UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].z));
		}
	}
	return true;
}

bool load(const char * path,std::vector<model>& mod, skeleton* skel)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_Triangulate
		//|aiProcess_JoinIdenticalVertices
	);
	if (!scene)
	{
		std::string errorString = importer.GetErrorString();
		printConsole(errorString);
		return false;
	}
	//harvest vertices, normals and UVs
	for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
		aiMesh* mesh = scene->mMeshes[j];
		mod.push_back(model());
		mod[j].vertices.reserve(scene->mMeshes[j]->mNumFaces * 3);
		mod[j].normals.reserve(scene->mMeshes[j]->mNumFaces * 3);
		mod[j].UVs.reserve(scene->mMeshes[j]->mNumFaces * 3);
		for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
			mod[j].vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[0]].x, mesh->mVertices[mesh->mFaces[i].mIndices[0]].y, mesh->mVertices[mesh->mFaces[i].mIndices[0]].z));
			mod[j].vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[1]].x, mesh->mVertices[mesh->mFaces[i].mIndices[1]].y, mesh->mVertices[mesh->mFaces[i].mIndices[1]].z));
			mod[j].vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[2]].x, mesh->mVertices[mesh->mFaces[i].mIndices[2]].y, mesh->mVertices[mesh->mFaces[i].mIndices[2]].z));

			mod[j].normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[0]].x, mesh->mNormals[mesh->mFaces[i].mIndices[0]].y, mesh->mNormals[mesh->mFaces[i].mIndices[0]].z));
			mod[j].normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[1]].x, mesh->mNormals[mesh->mFaces[i].mIndices[1]].y, mesh->mNormals[mesh->mFaces[i].mIndices[1]].z));
			mod[j].normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[2]].x, mesh->mNormals[mesh->mFaces[i].mIndices[2]].y, mesh->mNormals[mesh->mFaces[i].mIndices[2]].z));

			mod[j].UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].z));
			mod[j].UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].z));
			mod[j].UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].z));
		}
		if (mesh->mNumBones > animations::maxNumBones) {
			animations::maxNumBones = mesh->mNumBones;
		}
	}
	//harvest bone IDs and Weights
	for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[i];
		mod[i].boneIDWeights.resize(mod[i].vertices.size());
		mod[i].boneWeightsContigious.resize(mod[i].vertices.size());
		mod[i].boneIndicesContigious.resize(mod[i].vertices.size());
		mod[i].bones.resize(mesh->mNumBones, bone());
		mod[i].boneOffsetMatricesContigious.resize(mesh->mNumBones, glm::mat4(1.0f));
		for (int j = 0; j < mesh->mNumBones;j++) {
			for (int k = 0; k < mesh->mBones[j]->mNumWeights; k++) {
				mod[i].boneIDWeights[mesh->mBones[j]->mWeights[k].mVertexId].push_back(std::make_pair(j, mesh->mBones[j]->mWeights[k].mWeight));
			}			
			mod[i].bones[j].offsetTransform = convertAiMat2GlmMat(mesh->mBones[j]->mOffsetMatrix);
			std::string tempname(mesh->mBones[j]->mName.C_Str());
			mod[i].bones[j].name = mesh->mBones[j]->mName.C_Str();
		}
		mod[i].boneOffsetMatricesContigious.resize(animations::maxNumBones, glm::mat4(1.0f));
		mod[i].bones.resize(animations::maxNumBones, bone());
	}
	//fill the boneIDs and Weights buffers to have exact four elements per entry
	for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
		for (int i = 0; i < mod[j].boneIDWeights.size(); i++) {
			std::sort(mod[j].boneIDWeights[i].begin(), mod[j].boneIDWeights[i].end(), [](auto m, auto n) {
				return m.second > n.second;
			});
			mod[j].boneIDWeights[i].resize(4, std::make_pair(0,0.0f));
			float sum = mod[j].boneIDWeights[i][0].second + mod[j].boneIDWeights[i][1].second + mod[j].boneIDWeights[i][2].second + mod[j].boneIDWeights[i][3].second;
			mod[j].boneIDWeights[i][0].second /= sum;
			mod[j].boneIDWeights[i][1].second /= sum;
			mod[j].boneIDWeights[i][2].second /= sum;
			mod[j].boneIDWeights[i][3].second /= sum;
			mod[j].boneWeightsContigious[i] = glm::vec4(
				mod[j].boneIDWeights[i][0].second,
				mod[j].boneIDWeights[i][1].second,
				mod[j].boneIDWeights[i][2].second,
				mod[j].boneIDWeights[i][3].second
			);
			mod[j].boneIndicesContigious[i] = glm::vec4(
				mod[j].boneIDWeights[i][0].first,
				mod[j].boneIDWeights[i][1].first,
				mod[j].boneIDWeights[i][2].first,
				mod[j].boneIDWeights[i][3].first
			);
		}
	}
	//fill the hierarhcy information (DFS exploration)
	aiNode* node = scene->mRootNode;
	std::vector<aiNode*> frontier;
	frontier.push_back(node);
	while (frontier.size() > 0) {
		node = frontier.back();
		frontier.pop_back();
		//check if the node exists in the node vector, using its name
		if (std::find_if(skel->nodes.begin(), skel->nodes.end(), [node](auto i) {
			return node->mName.C_Str() == i.name;
		}) == skel->nodes.end()) 
		{ //PPPUSH IT!
			skeletonNode skNode = skeletonNode();
			skNode.name = node->mName.C_Str();
			skNode.localTransform = convertAiMat2GlmMat(node->mTransformation);
			skel->nodes.push_back(skNode);
		}
		else {
			exit(-4);
		}
		//update the frontier
		for (int i = 0; i < node->mNumChildren; i++) {
			frontier.push_back(node->mChildren[i]);
		}
	}
	//we have the nodes, time to get relationships
	node = scene->mRootNode;
	frontier.clear();
	frontier.push_back(node);
	//assign the Root index
	std::vector<skeletonNode>::iterator ego = std::find_if(skel->nodes.begin(), skel->nodes.end(), [node](auto i) {
		return i.name == node->mName.C_Str();
	});
	int ego_index = std::distance(skel->nodes.begin(), ego);
	skel->root = ego_index;
	//assign the hips index
	std::vector<skeletonNode>::iterator hips = std::find_if(skel->nodes.begin(), skel->nodes.end(), [](auto i) {
		return i.name == "Hips"||i.name=="arissa_Hips";
	});
	skel->hips = std::distance(skel->nodes.begin(), hips);
	while (frontier.size()) {
		node = frontier.back();
		frontier.pop_back();
		//find the node index in the skel nodes
		std::vector<skeletonNode>::iterator ego =std::find_if(skel->nodes.begin(), skel->nodes.end(), [node](auto i) {
			return i.name == node->mName.C_Str();
		});
		//std::cout << "ego is" << ego->name<<std::endl;
		int ego_index = std::distance(skel->nodes.begin(), ego);
		//find the children of that node in the skel nodes and update their parent with the ego index
		for (int i = 0; i < node->mNumChildren; i++) {
			std::vector<skeletonNode>::iterator child = std::find_if(skel->nodes.begin(), skel->nodes.end(), [node,i](auto elem) {
				return elem.name == node->mChildren[i]->mName.C_Str();
			});
			int child_index = std::distance(skel->nodes.begin(), child);
			skel->nodes[child_index].parent = ego_index;
			skel->nodes[ego_index].children.push_back(child_index);
		}
		//check if there is a bone with the same name
		for (int i = 0; i < scene->mNumMeshes; i++) {
			std::vector<bone>::iterator bone= std::find_if(mod[i].bones.begin(), mod[i].bones.end(), [ego](auto elem) {
				return ego->name == elem.name;
			});
			if (bone != mod[i].bones.end()) {
				ego->modelBoneIndices.push_back(std::make_pair(i, std::distance(mod[i].bones.begin(), bone)));
				//set the node local trans to the bone Offset matrix
			}
		}
		//update the frontier
		for (int i = 0; i < node->mNumChildren; i++) {
			frontier.push_back(node->mChildren[i]);  
		}
	}
	//get the animation information
	skel->animations.resize(scene->mNumAnimations);
	for (int i = 0; i < scene->mNumAnimations; i++) {
		skel->animations[i].nodes.resize(skel->nodes.size());
		skel->animations[i].duration = scene->mAnimations[i]->mDuration;
		skel->animations[i].ticksPerSecond = scene->mAnimations[i]->mTicksPerSecond;
		for (int j = 0; j < scene->mAnimations[i]->mNumChannels;j++) {
			//find the skeleton node with the same name as the animation node 
			aiNodeAnim* node = scene->mAnimations[i]->mChannels[j];
			std::vector<skeletonNode>::iterator result = std::find_if(skel->nodes.begin(), skel->nodes.end(), [node](auto elem) {
				return node->mNodeName.C_Str() == elem.name;
			});
			if(result==skel->nodes.end()){
				std::cout << "okkay" << std::endl;
				exit(-7);
			}
			int animNodeIndex = std::distance(skel->nodes.begin(), result);
			skel->animations[i].nodes[animNodeIndex].name = node->mNodeName.C_Str();
			skel->animations[i].nodes[animNodeIndex].translations.resize(node->mNumPositionKeys);
			skel->animations[i].nodes[animNodeIndex].rotations.resize(node->mNumRotationKeys);
			skel->animations[i].nodes[animNodeIndex].scalings.resize(node->mNumScalingKeys);
			if (skel->animations[i].numKeyFrames < node->mNumPositionKeys) {
				skel->animations[i].numKeyFrames = node->mNumPositionKeys;
			}
			if (node->mNumPositionKeys > 0) {
				skel->animations[i].activeNodeIndex = animNodeIndex;
			}
			for (int k = 0; k < node->mNumPositionKeys; k++) {
				skel->animations[i].nodes[animNodeIndex].translations[k].first = convertAiV32GLMV3(node->mPositionKeys[k].mValue);
				skel->animations[i].nodes[animNodeIndex].translations[k].second = node->mPositionKeys[k].mTime;
			}
			for (int k = 0; k < node->mNumRotationKeys; k++) {
				skel->animations[i].nodes[animNodeIndex].rotations[k].first = convertAiQuart2GLMQuart(node->mRotationKeys[k].mValue);
				skel->animations[i].nodes[animNodeIndex].rotations[k].second = node->mRotationKeys[k].mTime;
			}
			for (int k = 0; k < node->mNumPositionKeys; k++) {
				skel->animations[i].nodes[animNodeIndex].scalings[k].first = convertAiV32GLMV3(node->mScalingKeys[k].mValue);
				skel->animations[i].nodes[animNodeIndex].scalings[k].second = node->mScalingKeys[k].mTime;
			}
		}
	}
	CalculateBoneMetaInfo(mod);
}

bool loadAnimations(std::vector<std::string> paths, std::vector<model>& mods, skeleton * finalSkel)
{
	Assimp::Importer importer;
	std::vector<skeleton> skels(paths.size());
	for (int path_ind = 0; path_ind < paths.size(); path_ind++) {
		const aiScene* scene = importer.ReadFile(paths[path_ind],
			aiProcess_Triangulate
			//|aiProcess_JoinIdenticalVertices
		);
		if (!scene)
		{
			std::string errorString = importer.GetErrorString();
			printConsole(errorString);
			return false;
		}
		skels[path_ind] = skeleton(*finalSkel);
		skeleton* skel = &skels[path_ind];
		//get the animation information
		skel->animations.resize(scene->mNumAnimations);
		for (int i = 0; i < scene->mNumAnimations; i++) {
			skel->animations[i].nodes.resize(skel->nodes.size());
			skel->animations[i].duration = scene->mAnimations[i]->mDuration;
			skel->animations[i].ticksPerSecond = scene->mAnimations[i]->mTicksPerSecond;
			for (int j = 0; j < scene->mAnimations[i]->mNumChannels; j++) {
				//find the skeleton node with the same name as the animation node 
				aiNodeAnim* node = scene->mAnimations[i]->mChannels[j];
				std::vector<skeletonNode>::iterator result = std::find_if(skel->nodes.begin(), skel->nodes.end(), [node](auto elem) {
					return node->mNodeName.C_Str() == elem.name;
				});
				if (result == skel->nodes.end()) {
					exit(-9);
				}
				int animNodeIndex = std::distance(skel->nodes.begin(), result);
				skel->animations[i].nodes[animNodeIndex].name = node->mNodeName.C_Str();
				skel->animations[i].nodes[animNodeIndex].translations.resize(node->mNumPositionKeys);
				skel->animations[i].nodes[animNodeIndex].rotations.resize(node->mNumRotationKeys);
				skel->animations[i].nodes[animNodeIndex].scalings.resize(node->mNumScalingKeys);
				if (skel->animations[i].numKeyFrames < node->mNumPositionKeys) {
					skel->animations[i].numKeyFrames = node->mNumPositionKeys;
				}
				if (node->mNumPositionKeys > 0) {
					skel->animations[i].activeNodeIndex = animNodeIndex;
				}
				for (int k = 0; k < node->mNumPositionKeys; k++) {
					skel->animations[i].nodes[animNodeIndex].translations[k].first = convertAiV32GLMV3(node->mPositionKeys[k].mValue);
					skel->animations[i].nodes[animNodeIndex].translations[k].second = node->mPositionKeys[k].mTime;
				}
				for (int k = 0; k < node->mNumRotationKeys; k++) {
					skel->animations[i].nodes[animNodeIndex].rotations[k].first = convertAiQuart2GLMQuart(node->mRotationKeys[k].mValue);
					skel->animations[i].nodes[animNodeIndex].rotations[k].second = node->mRotationKeys[k].mTime;
				}
				for (int k = 0; k < node->mNumPositionKeys; k++) {
					skel->animations[i].nodes[animNodeIndex].scalings[k].first = convertAiV32GLMV3(node->mScalingKeys[k].mValue);
					skel->animations[i].nodes[animNodeIndex].scalings[k].second = node->mScalingKeys[k].mTime;
				}
			}
		}
		for (int i = 0; i < skel->animations.size(); i++) {
			//fill animation name and push to final skel 
			size_t res=paths[path_ind].find_last_of("/");
			skel->animations[i].name = paths[path_ind].substr(res+1,paths[path_ind].length()-res-5);
			finalSkel->animations.push_back(skel->animations[i]);
		}
	}
	return true;
}

bool loadHand(const char* path,std::vector<model>& mods,skeleton& skel) {
	//Assimp::Importer importer;
	//// And have it read the given file with some example postprocessing
	//// Usually - if speed is not the most important aspect for you - you'll 
	//// propably to request more postprocessing than we do in this example.
	//const aiScene* scene = importer.ReadFile(path,
	//	aiProcess_Triangulate
	//	//|aiProcess_JoinIdenticalVertices
	//);
	//if (!scene)
	//{

	//	std::string errorString = importer.GetErrorString();
	//	printConsole(errorString);
	//	return false;
	//}
	////print mesh information
	//std::cout <<"there are " <<scene->mNumMeshes<<" meshes." << std::endl;
	//int indexOffset = 0;
	//for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
	//	aiMesh* mesh = scene->mMeshes[j];
	//	std::cout << "\tname:" << mesh->mName.C_Str() << " num of vertices " << mesh->mNumVertices << std::endl;
	//	//create the bone here
	//	bone bono; bono.name = mesh->mName.C_Str();
	//	bono.localTransform = glm::mat4(1.0f);
	//	skeletonNode node;
	//	node.data = bono;
	//	node.parent = -1;
	//	//bono.end = mod.vertices.size();
	//	//pack bono as a node to the skeleton
	//	skel.nodes.push_back(node);
	//	model mod;
	//	for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
	//		mod.vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[0]].x, mesh->mVertices[mesh->mFaces[i].mIndices[0]].y, mesh->mVertices[mesh->mFaces[i].mIndices[0]].z));
	//		mod.vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[1]].x, mesh->mVertices[mesh->mFaces[i].mIndices[1]].y, mesh->mVertices[mesh->mFaces[i].mIndices[1]].z));
	//		mod.vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[2]].x, mesh->mVertices[mesh->mFaces[i].mIndices[2]].y, mesh->mVertices[mesh->mFaces[i].mIndices[2]].z));

	//		mod.normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[0]].x, mesh->mNormals[mesh->mFaces[i].mIndices[0]].y, mesh->mNormals[mesh->mFaces[i].mIndices[0]].z));
	//		mod.normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[1]].x, mesh->mNormals[mesh->mFaces[i].mIndices[1]].y, mesh->mNormals[mesh->mFaces[i].mIndices[1]].z));
	//		mod.normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[2]].x, mesh->mNormals[mesh->mFaces[i].mIndices[2]].y, mesh->mNormals[mesh->mFaces[i].mIndices[2]].z));

	//		mod.UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].z));
	//		mod.UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].z));
	//		mod.UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].z));
	//	}
	//	mods.push_back(mod);
	//}
	////assign parents (hardcoded)
	//for (int i = 0; i < skel.nodes.size(); i++) {
	//	skeletonNode& node = skel.nodes[i];
	//	//palm
	//	if (node.data.name == "carpals.001") {
	//		node.parent = -1;
	//	}
	//	//thumb
	//	else if (node.data.name == "mcarpal1.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "carpals.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "ProxPhalth.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "mcarpal1.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "DisPhalthm.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "ProxPhalth.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	//index
	//	else if (node.data.name == "mcarpal2.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "carpals.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "ProxPhal2.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "mcarpal2.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "MidPhal2.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "ProxPhal2.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "DistPhal2.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "MidPhal2.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	//middle
	//	else if (node.data.name == "mcarpal3.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "carpals.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "ProxPhal3.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "mcarpal3.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "MidPhal3.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "ProxPhal3.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "DistPhal3.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "MidPhal3.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	//small
	//	else if (node.data.name == "mcarpal4.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "carpals.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "ProxPhal4.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "mcarpal4.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "MidPhal4.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "ProxPhal4.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "DistPhal4.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "MidPhal4.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "mcarpal5.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "carpals.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "ProxPhal5.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "mcarpal5.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "MidPhal5.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "ProxPhal5.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//	else if (node.data.name == "DistPhal5.001") {
	//		int parent_index = std::distance(skel.nodes.begin(), std::find_if(skel.nodes.begin(), skel.nodes.end(), [](auto i) {if (i.data.name == "MidPhal5.001")return true; return false; }));
	//		node.parent = parent_index;
	//	}
	//}
	////assign children based on parents info
	//for (int i = 0; i < skel.nodes.size(); i++) {
	//	if (skel.nodes[i].parent == -1)continue;
	//	skeletonNode& parentNode = skel.nodes[skel.nodes[i].parent];
	//	//at node index as a child in the list of the parent
	//	parentNode.children.push_back(i);
	//}


	////assign reference points

	//for (int i = 0; i < skel.nodes.size(); i++) {
	//	if (skel.nodes[i].parent == -1)continue;
	//	//get the node and its parent
	//	skeletonNode& node = skel.nodes[i];
	//	skeletonNode& parentNode = skel.nodes[node.parent];
	//	//find the point on parent with the minimum distance from the node
	//	int bestIndex = -1; float bestDistance = std::numeric_limits<float>::max();
	//	for (int j = 0; j < mods[node.parent].vertices.size(); j++) {
	//		for (int k = 0; k < mods[i].vertices.size(); k++) {
	//			if (glm::length(mods[i].vertices[k] - mods[node.parent].vertices[j]) < bestDistance) {
	//				bestDistance = glm::length(mods[i].vertices[k] - mods[node.parent].vertices[j]);
	//				bestIndex = j;
	//			}
	//		}
	//	}
	//	node.data.referencePoint = mods[node.parent].vertices[bestIndex];
	//	node.data.localTransform = glm::mat4(1.0f);
	//} 
	//std::cout << "mkay" << std::endl;
return false;
}

bool loadAnimationList(const char * path, std::vector<std::string>& paths)
{
	std::ifstream file(std::string(path)+"AnimationList.txt");
	std::string line;
	if (!file.is_open()) {
		return false;
	}
	while (std::getline(file, line))
	{
		paths.push_back(std::string(path) + line);
	}
	file.close();
	return true;
}

bool loadHierarchy(const char * path,const char* hpath,std::vector<model>& mods, skeleton& skel) {
	//Assimp::Importer importer;
	//// And have it read the given file with some example postprocessing
	//// Usually - if speed is not the most important aspect for you - you'll 
	//// propably to request more postprocessing than we do in this example.
	//const aiScene* scene = importer.ReadFile(path,
	//	aiProcess_Triangulate
	//	//|aiProcess_JoinIdenticalVertices
	//);
	//if (!scene)
	//{

	//	std::string errorString = importer.GetErrorString();
	//	printConsole(errorString);
	//	return false;
	//}
	////print mesh information
	//std::cout << "there are " << scene->mNumMeshes << " meshes." << std::endl;
	//int indexOffset = 0;
	//for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
	//	aiMesh* mesh = scene->mMeshes[j];
	//	std::cout << "\t"<<j<<") name:" << mesh->mName.C_Str() << " num of vertices " << mesh->mNumVertices << std::endl;
	//	//create the bone here
	//	bone bono; bono.name = mesh->mName.C_Str();
	//	bono.localTransform = glm::mat4(1.0f);
	//	skeletonNode node;
	//	node.data = bono;
	//	node.parent = -1;
	//	//bono.end = mod.vertices.size();
	//	//pack bono as a node to the skeleton
	//	skel.nodes.push_back(node);
	//	model mod;
	//	for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
	//		mod.vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[0]].x, mesh->mVertices[mesh->mFaces[i].mIndices[0]].y, mesh->mVertices[mesh->mFaces[i].mIndices[0]].z));
	//		mod.vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[1]].x, mesh->mVertices[mesh->mFaces[i].mIndices[1]].y, mesh->mVertices[mesh->mFaces[i].mIndices[1]].z));
	//		mod.vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[2]].x, mesh->mVertices[mesh->mFaces[i].mIndices[2]].y, mesh->mVertices[mesh->mFaces[i].mIndices[2]].z));

	//		mod.normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[0]].x, mesh->mNormals[mesh->mFaces[i].mIndices[0]].y, mesh->mNormals[mesh->mFaces[i].mIndices[0]].z));
	//		mod.normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[1]].x, mesh->mNormals[mesh->mFaces[i].mIndices[1]].y, mesh->mNormals[mesh->mFaces[i].mIndices[1]].z));
	//		mod.normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[2]].x, mesh->mNormals[mesh->mFaces[i].mIndices[2]].y, mesh->mNormals[mesh->mFaces[i].mIndices[2]].z));

	//		mod.UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].z));
	//		mod.UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].z));
	//		mod.UVs.push_back(glm::vec3(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].y, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].z));
	//	}
	//	mods.push_back(mod);
	//}

	////find the length of each bone
	//for (int i = 0; i < mods.size(); i++) {
	//	glm::vec3 min=glm::vec3((std::numeric_limits<float>::max)(),(std::numeric_limits<float>::max)(),(std::numeric_limits<float>::max)());
	//	glm::vec3 max = -min;
	//	for (int j = 0; j < mods[i].vertices.size(); j++) {
	//		if (mods[i].vertices[j].x < min.x) {
	//			min.x = mods[i].vertices[j].x;
	//		}
	//		if (mods[i].vertices[j].y < min.y) {
	//			min.y = mods[i].vertices[j].y;
	//		}
	//		if (mods[i].vertices[j].z < min.z) {
	//			min.z = mods[i].vertices[j].z;
	//		}
	//		if (mods[i].vertices[j].x > max.x) {
	//			max.x = mods[i].vertices[j].x;
	//		}
	//		if (mods[i].vertices[j].y > max.y) {
	//			max.y = mods[i].vertices[j].y;
	//		}
	//		if (mods[i].vertices[j].z > max.z) {
	//			max.z = mods[i].vertices[j].z;
	//		}
	//	}
	//	
	//	float Lx = glm::abs(max.x - min.x);
	//	float Ly = glm::abs(max.y - min.y);
	//	float Lz = glm::abs(max.z - min.z);

	//	/*if (i == 92) {
	//		std::cout << "name is " << skel.nodes[i].data.name << std::endl;
	//		std::cout << "papa lengths are " << Lx << " and " << Ly << " and " << Lz << std::endl;
	//		std::cout << " maximas are " << glm::to_string(max) << " and " << glm::to_string(min) << std::endl;
	//	}*/
	//	skel.nodes[i].data.length = std::max(Lx, std::max(Ly, Lz));
	//}

	////read the hierarchy information from the hierarchy file
	//std::ifstream hfile(hpath,std::ifstream::in);
	//
	//char lineleft[256];
	//char lineright[256];
	//while (hfile.good())
	//{
	//	hfile.getline(lineleft, 256,':');
	//	hfile.getline(lineright, 256);
	//	std::string child(lineleft);
	//	std::string parent(lineright);
	//	//go on bambino, find your papa
	//	std::vector<skeletonNode>::iterator papa = std::find_if(skel.nodes.begin(), skel.nodes.end(), [parent](auto i) {return parent==i.data.name; });
	//	std::vector<skeletonNode>::iterator bambino = std::find_if(skel.nodes.begin(), skel.nodes.end(), [child](auto i) {return child == i.data.name; });
	//	if (papa != skel.nodes.end() && bambino !=skel.nodes.end()) {
	//		papa->children.push_back(std::distance(skel.nodes.begin(),bambino));
	//		bambino->parent = std::distance(skel.nodes.begin(),papa);
	//	}
	//	else {
	//		std::cout << "could not find nodes for " << child << " and " << parent << std::endl;
	//	}
	//}
	//hfile.close();
	//for (int i = 0; i < skel.nodes.size(); i++) {
	//	if (skel.nodes[i].parent == -1)continue;
	//	//get the node and its parent
	//	skeletonNode& node = skel.nodes[i];
	//	skeletonNode& parentNode = skel.nodes[node.parent];
	//	//find the point on parent with the minimum distance from the node
	//	int bestIndex = -1; float bestDistance = std::numeric_limits<float>::max();
	//	for (int j = 0; j < mods[node.parent].vertices.size(); j++) {
	//		for (int k = 0; k < mods[i].vertices.size(); k++) {
	//			if (glm::length(mods[i].vertices[k] - mods[node.parent].vertices[j]) < bestDistance) {
	//				bestDistance = glm::length(mods[i].vertices[k] - mods[node.parent].vertices[j]);
	//				bestIndex = j;
	//			}
	//		}
	//	}
	//	node.data.referencePoint = mods[node.parent].vertices[bestIndex];
	//	node.data.localTransform = glm::mat4(1.0f);
	//}
return false;
}