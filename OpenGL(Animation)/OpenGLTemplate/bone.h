#pragma once
#include <string>
#include <vector>
#include <glm\mat4x4.hpp>
#include <glm\gtc\quaternion.hpp>

#include "MotionStateMachine.h"

struct AABB {
	glm::vec3 center=glm::vec3(0);
	glm::vec3 size = glm::vec3(0);
	glm::vec3 axisA = glm::vec3(0);
	glm::vec3 axisB = glm::vec3(0);
	AABB(glm::vec3 min, glm::vec3 max);
	AABB() { ; }
};

struct bone {
	std::string name;
	float length = 0.0f;
	glm::vec3 referencePoint;
	glm::mat4 offsetTransform;
	AABB bbox;
	glm::vec3 head;
	glm::vec3 tail;
};

struct RootMotion {
public:
	glm::vec3 root_translation;
	float root_RotY;
	float root_RotYq;
	glm::fquat root_rotation;
	glm::vec3 root_scale;
	glm::vec3 previous_translation;
	float previous_RotY;
	float previous_RotYq;
	glm::vec3 previous_scale;
	glm::fquat previous_rotation;
};


struct skeletonNode {
	std::string name;
	std::vector<std::pair<int, int>> modelBoneIndices;
	int parent=-1;
	std::vector<int> children;
	glm::mat4 localTransform=glm::mat4(1.0f);
	glm::mat4 globalTransform=glm::mat4(1.0f); 
};

class animationNode {
public:
	std::string name;
	std::vector<std::pair<glm::vec3,float>> translations;
	std::vector<std::pair<glm::fquat,float>> rotations;
	std::vector<std::pair<glm::vec3,float>> scalings;
};

class animation {
public:
	std::string name;
	double duration;
	double ticksPerSecond;
	int numKeyFrames = 0;
	int activeNodeIndex = -1;//this is just an index to a node that is affected by the animation
	std::vector<animationNode> nodes;
};

class skeleton
{
public:
	int root;
	int hips;
	bool newPlay = false;
	RootMotion rootMotion;
	std::vector<skeletonNode> nodes;
	std::vector<animation> animations;
	MotionStateMachine msm;
	void initMSM();
};


