
#include "light.h"
#include <GL/glew.h>
#include <glm/matrix.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include "global.h"
#include <iostream>

void light::BindShadowMap() {
	this->map.width = 4096;
	this->map.height = 4096;
	glGenFramebuffers(1, &this->map.frameBufferID);
	//depth
	glGenTextures(1, &this->map.depthMapID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->map.depthMapID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,this->map.width, this->map.height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//backface depth
	glGenTextures(1, &this->map.backfaceDepthMapID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->map.backfaceDepthMapID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->map.width, this->map.height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//color
	glGenTextures(1, &this->map.colorMapID);
	glBindTexture(GL_TEXTURE_2D, this->map.colorMapID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, this->map.width, this->map.height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


	glBindFramebuffer(GL_FRAMEBUFFER, this->map.frameBufferID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,this->map.depthMapID, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->map.colorMapID, 0);
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void light::StartFirstPass() {
	glViewport(0, 0, this->map.width, this->map.height);
	glBindFramebuffer(GL_FRAMEBUFFER, this->map.frameBufferID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->map.depthMapID, 0);
	glClear(GL_DEPTH_BUFFER_BIT);
	//configure shader n matrices
	GLfloat near_plane = 1.0f, far_plane = 150.0f;
	glm::mat4 lightProjection;
	if(lightType==ORTHO)
		lightProjection= glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	else if(lightType==PERSP)
		lightProjection= glm::perspective(30.0f, 1.0f, near_plane, far_plane);
	lightView = glm::lookAt(position, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	lightSpace = lightProjection*lightView;
	//for normal mapping of the windows
	glm::mat4 lightModel = glm::mat4(glm::vec4(1, 0, 0, position.x), glm::vec4(0, 1, 0, position.y), glm::vec4(0, 0, 1, position.z), glm::vec4(0, 0, 0, 1));
	glUseProgram(this->depthMapShaderID);
	GLuint lightSpaceMatrixLocation = glGetUniformLocation(this->depthMapShaderID, "lightSpaceMatrix");
	glUniformMatrix4fv(lightSpaceMatrixLocation, 1, GL_FALSE, &(lightSpace[0][0]));
}

void light::SetLightProjectionPerspective() {
	lightType = PERSP;

}

void light::SetLightProjectionOrthographic() {
	lightType = ORTHO;
}

void light::EndFirstPass() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void light::StartSecondPass() {
	glViewport(0, 0, this->map.width, this->map.height);
	glBindFramebuffer(GL_FRAMEBUFFER, this->map.frameBufferID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->map.backfaceDepthMapID, 0);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void light::EndSecondPass() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void light::StartThirdPass() {
	glViewport(0, 0, viewport::width,viewport::height);
	glBindTexture(GL_TEXTURE_2D, this->map.depthMapID);
}

void light::EndThirdPass() {
	glBindFramebuffer(GL_FRAMEBUFFER, map.frameBufferID);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glViewport(0, 0, viewport::width, viewport::height);
}