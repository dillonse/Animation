#include "model.h"

void model::computeTangentBasis()
{
	for (int i = 0; i < vertices.size(); i += 3) {

		// Shortcuts for vertices
		glm::vec3 & v0 = vertices[i + 0];
		glm::vec3 & v1 = vertices[i + 1];
		glm::vec3 & v2 = vertices[i + 2];

		// Shortcuts for UVs
		glm::vec2 & uv0 = UVs[i + 0];
		glm::vec2 & uv1 = UVs[i + 1];
		glm::vec2 & uv2 = UVs[i + 2];

		// Edges of the triangle : postion delta
		glm::vec3 deltaPos1 = v1 - v0;
		glm::vec3 deltaPos2 = v2 - v0;

		// UV delta
		glm::vec2 deltaUV1 = uv1 - uv0;
		glm::vec2 deltaUV2 = uv2 - uv0;
		
		float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
		glm::vec3 tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y)*r;
		glm::vec3 bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x)*r;

		tangent=glm::normalize(tangent);
		bitangent = glm::normalize(bitangent);
		// Set the same tangent for all three vertices of the triangle.
		// They will be merged later, in vboindexer.cpp
		tangents.push_back(tangent);
		tangents.push_back(tangent);
		tangents.push_back(tangent);

		// Same thing for binormals
		bitangents.push_back(bitangent);
		bitangents.push_back(bitangent);
		bitangents.push_back(bitangent);
	}
}

void model::computeCentroid() {
	centroid = glm::vec3(0, 0, 0);
	for (int i = 0; i < vertices.size(); i ++) {
		centroid += vertices[i];
	}
	centroid /= vertices.size();
}

void model::flipFaceOrientation(){
	for (int i = 0; i < vertices.size(); i += 3) {
		//swap the first two vertices
		glm::vec3 swap = vertices[i];
		vertices[i] = vertices[i + 1];
		vertices[i + 1] = swap;
	}
	for (int i = 0; i < normals.size(); i += 3) {
		glm::vec3 swap = normals[i];
		normals[i] = normals[i + 1];
		normals[i + 1] = swap;
		normals[i] *= -1;
		normals[i + 1] *= -1;
		normals[i + 2] *= -1;
	}
	for (int i = 0; i < UVs.size(); i+=2) {
		glm::vec2 swap = UVs[i];
		UVs[i] = UVs[i + 1];
		UVs[i + 1] = swap;
	}
	for (int i = 0; i < tangents.size(); i+=3) {
		glm::vec3 swap = tangents[i];
		tangents[i] = tangents[i + 1];
		tangents[i + 1] = swap;
		tangents[i] *= -1;
		tangents[i + 1] *= -1;
		tangents[i + 2] *= -1;
	}
	for (int i = 0; i < bitangents.size(); i+=3) {
		glm::vec3 swap = bitangents[i];
		bitangents[i] = bitangents[i + 1];
		bitangents[i + 1] = swap;
		bitangents[i] *= -1;
		bitangents[i + 1] *= -1;
		bitangents[i + 2] *= -1;
	}
}

void model::scale(glm::vec3 scale)
{
	for (int i = 0; i < vertices.size(); i++) {
		vertices[i] = vertices[i] * scale;
	}
}

void model::swapYZ() {
	for (int i = 0; i < vertices.size(); i++) {
		std::swap(vertices[i].y, vertices[i].z);
	}
}

void model::translate(glm::vec3 translation) {
	for (int i = 0; i < vertices.size(); i++) {
		vertices[i] = vertices[i] + translation;
	}
}

bool model::isPositive() {
	for (int i = 0; i < vertices.size(); i++) {
		if (vertices[i].x < 0 || vertices[i].y < 0 || vertices[i].z < 0)
			return false;
	}
	return true;
}
