#pragma once
#include <GL/glew.h>
#include <glm/vec3.hpp>
struct material {
	GLuint DiffuseMapID=-1;
	GLuint NormalMapID=-1;
	GLuint SpecularMapID=-1;
	GLuint SkyboxMapID = -1;
	GLuint lightShaderID = -1;
	GLuint objectShaderID = -1;
	GLuint ShaderID = -1;
	GLfloat AlphaValue = 1.0;
	glm::vec3 color;
	glm::vec3 normal;
	GLint choice = 0;
	void BindTextures();
	void BindValues();
	void SwitchToLight();
	void SwitchToObject();
};