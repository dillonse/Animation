#pragma once
#include <vector>
#include <algorithm>
struct MSMNode {
	std::string name;
	int animation_index;
	std::vector<int> incoming;
	std::vector<int> outcoming;
	std::vector<bool> out_conditions;
	std::vector<float> out_time;
	std::vector<float> exitTime;
	int compolsury=-1;
};

class MotionStateMachine {
public:
	int currentNode=0;
	int previousNode = 0;
	float transitionPercentage = 0.0f;
	std::vector<MSMNode> nodes;
	bool setCurrentNode(std::string n);
	bool initTransition(std::string n1,std::string n2);
	bool setTransition(std::string n1, std::string n2, bool flag);
	bool setTransitionDuration(std::string n1, std::string n2, float time);
	bool setTransitionExitTime(std::string n1, std::string n2, float time);
	bool setCompolsury(std::string n1, std::string n2);
	void update();
	float timer=0.0f;
};