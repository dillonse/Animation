#pragma once

#include <vector>

#include "VolumeModel.h"
#include "ParticleSystem.h"
#include "RigidbodySystem.h"
class SurfaceModel
{
public:
	SurfaceModel(VolumeModel& volumeModel);
	SurfaceModel() { ; };
	model mod;
	void addForce(glm::vec3 cart_loc, float forceMagn);
	float getHeight(glm::vec3 cart_loc);
	void updateHeights();
	void updateNormals();
	void updateExternalPressures();
	void updateVerticalVelocities();
	void updateHorizontalVelocities();
	void updateParticleSystem(ParticleSystem& system);
	void updateCollisions(RigidbodySystem& system);
	std::vector<std::vector<float>> heightMap;
	std::vector<std::vector<glm::vec3>> normalMap;
	std::vector<std::vector<float>> forces;
	std::vector<std::vector<float>> verticalVelocities;
	std::vector < std::vector<glm::vec2>> horizontalVelocities;
	VolumeModel* volumeModel;
	int dimension = 0;
	void Render(material mat);
};
